var gulp = require('gulp');
var sass = require('gulp-sass');
var gutil = require('gulp-util');
var browserSync = require('browser-sync').create();

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: './'
        },
        notify: false
    });
});

gulp.task('watch', ['browserSync'], function() {
    gulp.watch('custom/scss/**/*.+(scss|sass)', ['sass']);
    gulp.watch('**/*.html', browserSync.reload);
    gulp.watch('**/*.js', browserSync.reload);
});

gulp.task('sass', function() {

    var s = sass();

    s.on('error', function(e) {
        gutil.log(e);
        s.end();
    });

    return gulp.src('custom/scss/**/*.+(scss|sass)')
            .pipe(s)
            .pipe(gulp.dest('dist/css'))
            .pipe(browserSync.reload({
                stream: true
            }));
});