var formKalkulatorZakat = document.getElementById('form-kalkulator-zakat');
var btnSubmit = $(formKalkulatorZakat.submit);
var type = null;

formInit();

$(formKalkulatorZakat).on('focus', 'input', function (ev) {
    var val = ev.target.value;
    if (val < 1) {
        $(this).val('');
    }
});
$(formKalkulatorZakat).on('focusout', 'input', function (ev) {
    if (ev.target.value < 1) {
        $(ev.target).val(0);
    }
});

$('#kalkulator-tab a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
});

// store the currently selected tab in the hash value
$("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
    var id = $(e.target).attr("href").substr(1);
    var newBtn = $(e.target);
    var old = $(e.relatedTarget);
    
    window.location.hash = id;
    formKalkulatorZakat.type.value = newBtn.attr('type');

    switch (newBtn.attr('type')) {
        case 'penghasilan':
            hitungZakatPenghasilan();
            break;
        case 'perdagangan':
            hitungZakatPerdagangan();
            break;
        case 'tabungan':
            hitungZakatTabungan();
            break;
        case 'emas':
            hitungZakatEmas();
            break;
        default:
            console.log('Error type kalkulator zakat');
            break;
    }
});

// on load of the page: switch to the currently selected tab
var hash = window.location.hash || '#penghasilan';
$('#kalkulator-tab a[href="' + hash + '"]').tab('show');

function btnDisabled(disable) {
    disable ? btnSubmit.attr('disabled', true) : btnSubmit.attr('disabled', false);
}

function formInit() {
    formKalkulatorZakat.reset();
    btnDisabled(true);
    $('.harga-beras').val(hargaBeras);
    $('.nishab-beras').val(nishabBerasHarga);
    $('.harga-emas').val(hargaEmas);
    $('.nishab-emas').val(nishabEmasHarga);
}

function hitungZakatPenghasilan() {
    $(formKalkulatorZakat).on('keyup', 'input', function (ev) {
        var data = {
            penghasilan: parseInt(formKalkulatorZakat.z_1_penghasilan.value),
            penghasilanLain: parseInt(formKalkulatorZakat.z_1_pendapatan_lain.value),
            pengeluaran: parseInt(formKalkulatorZakat.z_1_pengeluaran.value),
        }

        var jml = (data.penghasilan + data.penghasilanLain) - data.pengeluaran;
        console.log(jml);
        if (jml) {
            (jml >= 1000000) ? btnDisabled(false): btnDisabled(true);
            formKalkulatorZakat.z_1_wajib_zakat.value = (jml >= nishabBerasHarga) ? 'YA' : 'TIDAK';
            formKalkulatorZakat.z_1_zakat_bln.value = jml * .025;
            formKalkulatorZakat.z_1_zakat_thn.value = (jml * .025) * 12;
        }
    });
}

function hitungZakatPerdagangan() {
    $(formKalkulatorZakat).on('keyup', 'input', function (ev) {
        var data = {
            modal: parseInt(formKalkulatorZakat.z_2_modal.value),
            keuntungan: parseInt(formKalkulatorZakat.z_2_untung.value),
            piutangDagang: parseInt(formKalkulatorZakat.z_2_piutang.value),
            hutang: parseInt(formKalkulatorZakat.z_2_hutang.value),
            rugi: parseInt(formKalkulatorZakat.z_2_rugi.value),
        }

        var jml = (data.modal + data.keuntungan + data.piutangDagang) - (data.hutang + data.rugi);

        if (jml) {
            (jml >= 1000000) ? btnDisabled(false): btnDisabled(true);
            formKalkulatorZakat.z_2_wajib_zakat.value = (jml >= nishabEmasHarga) ? 'YA' : 'TIDAK';
            formKalkulatorZakat.z_2_zakat.value = jml * .025;
        }
    });
}

function hitungZakatTabungan() {
    $(formKalkulatorZakat).on('keyup', 'input', function (ev) {
        var data = {
            saldo: parseInt(formKalkulatorZakat.z_3_saldo.value),
            bagiHasil: parseInt(formKalkulatorZakat.z_3_bagi_hasil.value),
        }

        var jml = data.saldo + data.bagiHasil;

        if (jml) {
            (jml >= 1000000) ? btnDisabled(false): btnDisabled(true);
            formKalkulatorZakat.z_3_wajib_zakat.value = (jml >= nishabEmasHarga) ? 'YA' : 'TIDAK';
            formKalkulatorZakat.z_3_zakat.value = jml * .025;
        }
    });
}

function hitungZakatEmas() {
    $(formKalkulatorZakat).on('keyup', 'input', function (ev) {
        var jml = formKalkulatorZakat.z_4_emas.value * hargaEmas;

        if (jml) {
            (jml >= 1000000) ? btnDisabled(false): btnDisabled(true);
            formKalkulatorZakat.z_4_wajib_zakat.value = (jml >= nishabEmasHarga) ? 'YA' : 'TIDAK';
            formKalkulatorZakat.z_4_zakat.value = jml * .025;
        }
    });
}