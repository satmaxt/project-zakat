var formPembayaran = document.getElementById('form-pembayaran');

$('input[name="pembayaran"]').on('ifChecked', function(ev) {
    var price = ev.target.getAttribute('price');
    formPembayaran.besar_zakat.value=price;
});

$('input[name="metode_pembayaran"]').on('ifChecked', function(ev) {
    var type = ev.target.getAttribute('value');
    var infoMetodePembayaran = $('#info-metode-pembayaran');
    var dataInfo = dataBank[type];
    infoMetodePembayaran.html(`
    <strong>${dataInfo.bank}</strong><br>
    Nomor Rekening: ${dataInfo.rek} <br>
    Atas Nama: ${dataInfo.name}
    `);
    infoMetodePembayaran.show();
});

$('input[name="pembayaran"][value="bln"]').iCheck('check');