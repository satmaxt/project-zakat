var wow = new WOW();
var formKalkulatorZakat = document.getElementById('form-kalkulator-zakat');
var btnSubmit = $(formKalkulatorZakat.submit);
var type = null;

wow.init();
formInit();

$(window).scroll(function (event) {
    if ($(this).scrollTop() > 400) {
        $('#toTop').attr('style', '');
        $('#toTop').removeClass('rotateOutDownLeft animated').addClass('rotateInDownRight animated');
    } else {
        $('#toTop').removeClass('rotateInDownRight animated').addClass('rotateOutDownLeft animated');
        
    }
});

jQuery(document).ready(function ($) {

    $('a.scroll-link').click(function (e) {
        e.preventDefault();
        $id = $(this).attr('href');
        $('body,html').animate({
            scrollTop: $($id).offset().top - 20
        }, 750);
    });

});

modalOnz();

$(formKalkulatorZakat).on('focus', 'input', function (ev) {
    var val = ev.target.value;
    if (val < 1) {
        $(this).val('');
    }
});
$(formKalkulatorZakat).on('focusout', 'input', function (ev) {
    if (ev.target.value < 1) {
        $(ev.target).val(0);
    }
});

$('#kalkulator-tab .btn-green').tab('show');
$('#kalkulator-tab a[data-toggle="pill"]').on('shown.bs.tab', function (ev) {
    var newBtn = $(ev.target);
    var old = $(ev.relatedTarget);

    formKalkulatorZakat.type.value = newBtn.attr('type');

    switch (newBtn.attr('type')) {
        case 'penghasilan':
            hitungZakatPenghasilan();
            break;
        case 'perdagangan':
            hitungZakatPerdagangan();
            break;
        case 'tabungan':
            hitungZakatTabungan();
            break;
        case 'emas':
            hitungZakatEmas();
            break;
        default:
            console.log('Error type kalkulator zakat');
            break;
    }

    if (!newBtn.hasClass('btn-green')) {
        formInit();
        newBtn.removeClass('btn-green-light');
        newBtn.addClass('btn-green');
        old.removeClass('btn-green');
        old.addClass('btn-green-light');
    }
});

function modalOnz() {
    $('[data-toggle="modal-onz"]').click(function (ev) {
        ev.preventDefault();
        $('body').addClass('modal-open');
        var target = $($(this).attr('href'));
        target.addClass('show animated');
    });

    $('[data-toggle="modal-onz-close"]').click(function (ev) {
        ev.preventDefault();
        var parent = $(this).parentsUntil('.onz-modal').parent();
        parent.removeClass('fadeInDown').addClass('fadeOutUp');
        setTimeout(function () {
            parent.removeClass('fadeOutUp')
                .removeClass('show')
                .removeClass('animated')
                .addClass('fadeInDown');
        }, 1000);
        $('body').removeClass('modal-open');
    });
}

function btnDisabled(disable) {
    disable ? btnSubmit.attr('disabled', true) : btnSubmit.attr('disabled', false);
}

function formInit() {
    formKalkulatorZakat.reset();
    btnDisabled(true);
    $('.harga-beras').val(hargaEmas);
    $('.nishab-beras').val(nishabEmasHarga);
    $('.harga-emas').val(hargaEmas);
    $('.nishab-emas').val(nishabEmasHarga);
}

function hitungZakatPenghasilan() {
    $(formKalkulatorZakat).on('keyup', 'input', function (ev) {
        var data = {
            penghasilan: parseInt(formKalkulatorZakat.z_1_penghasilan.value),
            penghasilanLain: parseInt(formKalkulatorZakat.z_1_pendapatan_lain.value),
            pengeluaran: parseInt(formKalkulatorZakat.z_1_pengeluaran.value),
        }

        var jml = (data.penghasilan + data.penghasilanLain) - data.pengeluaran;

        if (jml) {
            (jml >= 1000000) ? btnDisabled(false): btnDisabled(true);
            formKalkulatorZakat.z_1_wajib_zakat.value = (jml >= nishabEmasHarga) ? 'YA' : 'TIDAK';
            formKalkulatorZakat.z_1_zakat_bln.value = jml * .025;
            formKalkulatorZakat.z_1_zakat_thn.value = (jml * .025) * 12;
        }
    });
}

function hitungZakatPerdagangan() {
    $(formKalkulatorZakat).on('keyup', 'input', function (ev) {
        var data = {
            modal: parseInt(formKalkulatorZakat.z_2_modal.value),
            keuntungan: parseInt(formKalkulatorZakat.z_2_untung.value),
            piutangDagang: parseInt(formKalkulatorZakat.z_2_piutang.value),
            hutang: parseInt(formKalkulatorZakat.z_2_hutang.value),
            rugi: parseInt(formKalkulatorZakat.z_2_rugi.value),
        }

        var jml = (data.modal + data.keuntungan + data.piutangDagang) - (data.hutang + data.rugi);

        if (jml) {
            (jml >= 1000000) ? btnDisabled(false): btnDisabled(true);
            formKalkulatorZakat.z_2_wajib_zakat.value = (jml >= nishabEmasHarga) ? 'YA' : 'TIDAK';
            formKalkulatorZakat.z_2_zakat.value = jml * .025;
        }
    });
}

function hitungZakatTabungan() {
    $(formKalkulatorZakat).on('keyup', 'input', function (ev) {
        var data = {
            saldo: parseInt(formKalkulatorZakat.z_3_saldo.value),
            bagiHasil: parseInt(formKalkulatorZakat.z_3_bagi_hasil.value),
        }

        var jml = data.saldo + data.bagiHasil;

        if (jml) {
            (jml >= 1000000) ? btnDisabled(false): btnDisabled(true);
            formKalkulatorZakat.z_3_wajib_zakat.value = (jml >= nishabEmasHarga) ? 'YA' : 'TIDAK';
            formKalkulatorZakat.z_3_zakat.value = jml * .025;
        }
    });
}

function hitungZakatEmas() {
    $(formKalkulatorZakat).on('keyup', 'input', function (ev) {
        var jml = formKalkulatorZakat.z_4_emas.value * hargaEmas;

        if (jml) {
            (jml >= 1000000) ? btnDisabled(false): btnDisabled(true);
            formKalkulatorZakat.z_4_wajib_zakat.value = (jml >= nishabEmasHarga) ? 'YA' : 'TIDAK';
            formKalkulatorZakat.z_4_zakat.value = jml * .025;
        }
    });
}