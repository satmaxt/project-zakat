var gulp = require('gulp');
var sass = require('gulp-sass');
var gutil = require('gulp-util');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var browserSync = require('browser-sync').create();
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');

gulp.task('images', function(){
    return gulp.src('app/assets/images/**/*.+(png|jpg|gif|svg|jpeg)')
    .pipe(cache(imagemin()))
    .pipe(gulp.dest('dist/assets/images'));
});

gulp.task('fonts', function() {
    return gulp.src('app/assets/fonts/**/*')
    .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('useref', function(){
    return gulp.src('app/*.html')
      .pipe(useref())
      // Minifies only if it's a JavaScript file
      .pipe(gulpIf('*.js', uglify()))
      .pipe(gulp.dest('dist'))
      .pipe(gulpIf('*.css', cssnano()))
      .pipe(gulp.dest('dist'));
  });

gulp.task('publish', ['useref','fonts','images']);

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: 'app'
        },
        notify: false
    });
});

gulp.task('watch', ['browserSync'], function() {
    gulp.watch('app/assets/scss/**/*.+(scss|sass)', ['sass']);
    gulp.watch('app/**/*.html', browserSync.reload);
    gulp.watch('app/**/*.js', browserSync.reload);
});

gulp.task('sass', function() {

    var s = sass();

    s.on('error', function(e) {
        gutil.log(e);
        s.end();
    });

    return gulp.src('app/assets/scss/**/*.+(scss|sass)')
            .pipe(s)
            .pipe(gulp.dest('dist/assets/css'))
            .pipe(browserSync.reload({
                stream: true
            }));
});