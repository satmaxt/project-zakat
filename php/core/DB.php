<?php
namespace Core;

use PDO;

class DB
{
    private $pdo;
    private $host = "localhost";
    private $dbname = "onzakat";
    private $username = "root";
    private $password = "";
    private static $instance = null;

    public function __construct()
    {
        try {
            $this->pdo = new PDO(
                "mysql:host=$this->host;dbname=$this->dbname;charset=utf8",
                $this->username,
                $this->password
            );
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            
        } catch (PDOException $e) {
            die("Error: ".$e->getMessage());
        }
    }

    public static function conn() {
        return self::startConnection()->pdo;
    }

    public static function query($query, array $params = array()) {
        $q = self::conn()->prepare($query);
        $q->execute($params);
        return $q->lastInsertID();
    }

    public static function startConnection()
    {
        if(!isset(self::$instance)) {
            self::$instance = new DB;
        }
        return self::$instance;
    }
}