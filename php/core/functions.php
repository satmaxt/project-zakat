<?php

if (!defined('SITE_URL')) {
    define('SITE_URL', 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']));
}

if (!defined('ONZAKAT')) {
    define('ONZAKAT', time());
}

if(!function_exists('variableArray')) {
    function variableArray($data, $string) {
        preg_match_all('/\[([^\]]*)\]/', $string, $arr_matches, PREG_PATTERN_ORDER); 
        
        $return = $arr; 
        foreach($arr_matches[1] as $dimension) { $return = $return[$dimension]; }
    
        return $return; 
    }
}

if(!function_exists('redirectUnAuth')) {
	function redirectUnAuth()
	{
		if(isset($_SESSION['user'])) {
			$redirect = $_SESSION['user']->is_admin ? "?admin=dashboard" : "?member=dashboard";
			header("location: index.php".$redirect);
		}
	}
}

if(!function_exists('alert')) {
	function alert() {
		if(isset($_SESSION['flash_message'])) {
			$sess = $_SESSION['flash_message'];
			unset($_SESSION['flash_message']);
			require_once __DIR__.'/../partials/alert.php';
		}
	}
}

if(!function_exists('user')) {
	function user($username = false) {
		if(isset($_SESSION['user'])) {
			$q = Core\DB::conn()->prepare("SELECT * FROM users WHERE username = ?");
			$username = $username ? $username : $_SESSION['user']->username;
			$q->execute([
				$username
			]);

			$_SESSION['user'] = $q->fetch(PDO::FETCH_OBJ);

			return $_SESSION['user'];
		}
	}
}

if(!function_exists('adminDashboardData')) {
	function adminDashboardData()
	{
		$db = Core\DB::conn();

		$data['user'] = $db->query("SELECT id FROM users", PDO::FETCH_ASSOC)->rowCount();
		$data['pertanyaan'] = $db->query("SELECT id FROM pertanyaan", PDO::FETCH_ASSOC)->rowCount();
		$data['zakat'] = $db->query("SELECT id FROM zakat WHERE `status` = 'success'", PDO::FETCH_ASSOC)->rowCount();
		$data['konfirmasi'] = $db->query("SELECT id FROM zakat WHERE `status` != 'success'", PDO::FETCH_ASSOC)->rowCount();

		return $data;
	}
}

if(!function_exists('memberDashboardData')) {
	function memberDashboardData()
	{
		$db = Core\DB::conn();

		$data['pertanyaan'] = $db->query("SELECT id FROM pertanyaan WHERE user_id = ".user()->id, PDO::FETCH_ASSOC)->rowCount();
		$data['zakat'] = $db->query("SELECT id FROM zakat WHERE `status` = 'success' AND user_id = ".user()->id, PDO::FETCH_ASSOC)->rowCount();
		$data['konfirmasi'] = $db->query("SELECT id FROM zakat WHERE `status` != 'success' AND user_id = ".user()->id, PDO::FETCH_ASSOC)->rowCount();
		$data['pembayaran'] = $db->query("
			SELECT A.*, B.jenis AS jenis_bank FROM zakat AS A INNER JOIN rekening AS B ON B.id = A.rekening_id WHERE A.status != 'success' AND A.user_id = ".user()->id." ORDER BY A.pada DESC LIMIT 5
		", PDO::FETCH_OBJ)->fetchAll();

		return $data;
	}
}

if (!function_exists('updatePrice')) {
	function updatePrice($type)
	{
		$price = new Libraries\UpdatePrice;
		if($type == "beras") return (float)$price->getHargaBeras();
		else if($type == "emas") return (float)$price->getHargaEmas();
		else return false;
	}
}

if (!function_exists('setting')) {
	function setting($settingName, $value = true)
	{
		$q = Core\DB::conn()->prepare("SELECT setting_value FROM settings WHERE setting_name = ?");
		$q->bindParam(1, $settingName);
		if($q->execute()) {
			$result = $q->fetch(PDO::FETCH_OBJ);
			return $result->setting_value;
		}
		return null;
	}
}

if(!function_exists('word_limit')) {
	function word_limit($text, $limit) {
		if (str_word_count($text, 0) > $limit) {
			$words = str_word_count($text, 2);
			$pos = array_keys($words);
			$text = substr($text, 0, $pos[$limit]) . '...';
		}
		return $text;
	}
}