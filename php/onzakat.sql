-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 30, 2018 at 12:48 PM
-- Server version: 10.1.29-MariaDB-6
-- PHP Version: 7.2.7-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onzakat`
--

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(191) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `thumbnail` varchar(191) DEFAULT NULL,
  `description` text NOT NULL,
  `page_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `thumbnail`, `description`, `page_type`) VALUES
(1, 'Tentang On:Zakat', 'profil', 'uploads/pages/1530236541.jpg', '<p>Lorem ipsum dolor sit amet, pro etiam clita disputando ut, ei paulo semper dissentiunt sed. Consul veritus mediocritatem has at, ea suavitate accusamus vix, hinc melius blandit est ex. Duo dicat nihil ea, ei mel modus mediocrem maiestatis. Ei mel labore epicuri, hinc blandit has ei. Ea urbanitas mediocritatem qui, facilisi periculis explicari in vis. Impedit efficiendi cu per, dicunt commodo instructior et nec. Odio graece persequeris qui in. Suas viderer partiendo id est. Eu audire tibique nec. Ad mel atqui aeque essent, eripuit senserit no sea. Et odio deleniti consequat mei. Gloriatur sadipscing eu duo, natum repudiandae no vim, animal apeirian assueverit an mei. Ne wisi aeque eum, ne mutat definiebas cum. Veritus corpora an usu</p>', 'profile');

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan`
--

CREATE TABLE `pertanyaan` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `judul` varchar(191) NOT NULL,
  `detail` text NOT NULL,
  `jawaban` text,
  `pada` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pertanyaan`
--

INSERT INTO `pertanyaan` (`id`, `user_id`, `judul`, `detail`, `jawaban`, `pada`) VALUES
(8, 11, 'Nomor HP', 'Min saya minta nomor hp On:Zakat yang terbaru', 'Ini dia nomronya\r\n085863331766', '2018-06-30 04:44:22'),
(9, 18, 'Cara membayar', 'Bagaimana tatacara pembayaran di On:Zakat?', NULL, '2018-06-30 04:59:24');

-- --------------------------------------------------------

--
-- Table structure for table `rekening`
--

CREATE TABLE `rekening` (
  `id` int(11) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `nama` varchar(191) NOT NULL,
  `pemilik` varchar(191) NOT NULL,
  `nomor` varchar(50) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rekening`
--

INSERT INTO `rekening` (`id`, `jenis`, `nama`, `pemilik`, `nomor`, `deleted`) VALUES
(1, 'bca', 'Bank Central Asia', 'On:Zakat Corp.', '8272927162', 0),
(2, 'mandiri', 'Mandiri', 'On:Zakat Corp.', '992385728975382758235', 0),
(3, 'bjb', 'Bank Jabar (BJB)', 'On:Zakat Corp.', '823759278237435', 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `setting_name` varchar(50) NOT NULL,
  `setting_value` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `setting_name`, `setting_value`) VALUES
(1, 'site_name', 'On:Zakat'),
(2, 'email', 'admin@onzakat.com'),
(3, 'no_telp', '0858-6333-1766'),
(4, 'social_fb', 'onzakat'),
(5, 'social_twitter', 'onzakat'),
(6, 'social_gplus', 'onzakat');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jenis_kelamin` char(2) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` varchar(191) NOT NULL,
  `foto` varchar(191) NOT NULL,
  `telp` char(20) NOT NULL,
  `password` varchar(191) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `nama`, `jenis_kelamin`, `tanggal_lahir`, `alamat`, `foto`, `telp`, `password`, `is_admin`) VALUES
(11, 'admin@admin.com', 'administrator', 'Hermanito', 'L', '1999-07-12', 'Kp Selakopi RT 02', 'uploads/avatar04.png', '085863331766', '$2y$10$K77xjmFZOUGvmn576UftKugT5LbWBfClRouASNrgJ0UEwVQ3ubnya', 1),
(18, 'member@member.com', 'member', 'Diego Samad', 'L', '1999-02-03', '682 Ray Court, Fayetteville, NC', 'uploads/1530240048.jpg', '0858633317834', '$2y$10$5GJx1DjYlgUXcmwh7Qxp/eqb.xbsH02.QpMU6yLeNVao8GJbdZCpi', 0),
(19, 'member1@member.com', 'member2', 'Farhunnisa Yuniar', 'P', '1992-04-21', 'KP Gorontalos', 'uploads/1530322906.jpeg', '08667837577837', '$2y$10$erFc.OQgmLgJWTcQJPMvi.l66qvbOGMLzgvT5x4IpjyCUcwFirKOu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `zakat`
--

CREATE TABLE `zakat` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `jenis_zakat` varchar(50) NOT NULL,
  `jumlah` decimal(13,4) NOT NULL,
  `bukti_foto` varchar(191) NOT NULL,
  `rekening_id` int(11) NOT NULL,
  `monthly` tinyint(1) DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `pada` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `zakat`
--

INSERT INTO `zakat` (`id`, `user_id`, `jenis_zakat`, `jumlah`, `bukti_foto`, `rekening_id`, `monthly`, `status`, `pada`) VALUES
(2, 19, 'penghasilan', '2700000.0000', 'uploads/bukti/1530327213.jpg', 1, 0, 'waiting', '2018-06-30 02:42:18'),
(3, 11, 'emas', '1241000.0000', 'uploads/bukti/1530329010.jpg', 2, NULL, 'success', '2018-06-30 03:22:05'),
(4, 18, 'emas', '1241000.0000', 'uploads/bukti/1530334930.jpg', 2, NULL, 'success', '2018-06-30 04:56:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `rekening`
--
ALTER TABLE `rekening`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `zakat`
--
ALTER TABLE `zakat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `rekening_id` (`rekening_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `rekening`
--
ALTER TABLE `rekening`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `zakat`
--
ALTER TABLE `zakat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  ADD CONSTRAINT `pertanyaan_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `zakat`
--
ALTER TABLE `zakat`
  ADD CONSTRAINT `zakat_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `zakat_ibfk_2` FOREIGN KEY (`rekening_id`) REFERENCES `rekening` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
