<?php
namespace Libraries;

// API for Update Price Gold and Rice
// Gold: https://www.indogold.com/harga-emas-hari-ini
// Rice: https://bps.go.id/LinkTableDinamis/view/id/963

class UpdatePrice
{
    protected $url;
    protected $ch;
    protected $result;

    /**
     * Run initial code when class initiated
     * 
     * @param string $url
     */
    public function __construct($url = false)
    {
        if($url) $this->url = $url;
    }

    /**
     * For set url value
     * 
     * @param string $url
     * @return string $url
     */
    public function setUrl($url)
    {
        return $this->url = $url;
    }

    /**
     * For executing curl
     * 
     * @return void
     */
    public function execute()
    {
        $this->result = curl_exec($this->ch);
        if(!$this->result) throw new \Exception("Gagal mengambil data dengan curl", 503);
        
        curl_close($this->ch);
        return $this->result;
    }

    /**
     * Start standard curl
     * 
     * @param string optional $url
     * @return void
     */
    public function connect($url = false)
    {
        if($url) $this->url = $url;

        $this->ch = curl_init($this->url);
        curl_setopt($this->ch, CURLOPT_USERAGENT, "Googlebot/2.1 (http://www.googlebot.com/bot.html)");
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);

        return $this->execute();
    }

    /**
     * Start post curl with adding data
     * 
     * @param string $url
     * @param array $data
     * @return void
     */
    public function connectPost($url, array $data)
    {
        $this->ch = curl_init($url);
        curl_setopt($this->ch, CURLOPT_USERAGENT, "Googlebot/2.1 (http://www.googlebot.com/bot.html)");
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);

        return $this->execute();
    }

    /**
     * Get tables data from indonesian badan pengawas statistika
     * 
     * @return void
     */
    public function getTables()
    {
        $resultJSON = json_decode($this->connectPost(
            'https://bps.go.id/mod/params/returnParams.php', 
            [
                'idLink' => 963
            ]
        ), true);

        $params = [];

        // Converting array string to array
        foreach($resultJSON as $row) {
            if(is_array($row)) {
                unset($row[0]);
                $params[] = implode('', $row);
            } else {
                $params[] = $row;
            }
        }

        return $this->connectPost(
            'https://bps.go.id/mod/Layout/variabelView.php',
            [
                'valueDataSelect' => $params[7],
                'wilayahDataSelect' => $params[9],
                'keteranganDataSelect' => $params[8],
                'kirim' => '3',
                'layout' => $params[3],
            ]
        );
    }

    /**
     * Get gold price from Indo Gold
     * 
     * @return integer
     */
    public function getHargaEmas()
    {
        $html = str_get_html($this->connect('https://www.indogold.com/harga-emas-hari-ini'));
        $title = explode(' ', $html->find('title', 0)->innertext);

        // show error if page are not found
        if(in_array('404', $title)) throw new \Exception("Halaman tidak ditemukan", 503);
        
        $hargaEmas = $html->find("tr[class=odd]", 0)->find('td', 2)->innertext;

        return (int)str_replace(',', '', $hargaEmas);
    }

    /**
     * Get rice price from getTables()
     * 
     * @return integer
     */
    public function getHargaBeras()
    {
        $html = str_get_html($this->getTables());
        $table = $html->find('#tableRightBottom',0);
        $hargaBeras = $table->find('td', count($table->find('td'))-1)->innertext;
        return (int)str_replace(' ', '', $hargaBeras);
    }
}