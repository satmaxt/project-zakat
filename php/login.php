<?php
$page_title = "Masuk Akun - ";

require_once __DIR__.'/core/init.php';
require_once __DIR__.'/partials/admin/header-login.php';

redirectUnAuth();

if(isset($_POST['submit'])) {
    $user = Data\Auth::login($_POST['email'], $_POST['password']);
}

?>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="assets/index2.html"><b>On:</b>Zakat</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Masuk untuk mengakses area beranda</p>
    <?php alert(); ?>
    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
            <a href="register.php" class="text-center text-success">Belum punya akun?</a>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="submit" class="btn btn-success btn-block btn-flat">Masuk</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php
require_once __DIR__.'/partials/admin/footer-login.php';
?>