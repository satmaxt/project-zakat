<section class="header-content" style="background-image:url('assets/images/bg.jpg');background-size:cover;background-repeat: no-repeat;background-position-x: auto;background-position-y: 30%">
        <div style="background-color: rgba(0, 0, 0, 0.6);">
            <div class="container">
                <h2 class="title wow fadeInDown" data-wow-duration=".3s">Zakat
                    <strong>Mengubah</strong> Rezeki Menjadi
                    <strong>Berkah</strong>
                </h2>
                <p class="wow fadeIn">
                    ”Ambillah zakat dari sebagian harta mereka, dengan zakat itu kamu membersihkan dan mensucikan mereka dan do’akanlah mereka
                    karena sesungguhnya do’amu dapat memberi ketenangan bagi mereka. Dan Allah Maha Mendengar lagi Maha Mengetahui.”
                    <br />
                    <strong>(QS.At Taubah103)</strong>
                </p>
                <div class="text-center wow fadeInUp action" data-wow-duration=".3s" data-wow-delay=".2s">
                    <a href="#tentang-kami" class="btn btn-green-light scroll-link">Selengkapnya</a>
                    <a href="#kalkulator" class="btn btn-white-transparent scroll-link">Bayar Zakat</a>
                </div>
            </div>
        </div>
    </section>

    <section class="content py-100 bg-cloud" id="tentang-kami">
        <div class="container">
            <div class="content-title text-center wow fadeInUp" data-wow-duration=".5s">
                <h2><?= $profile->title ?></h2>
            </div>
            <div class="content-body text-center text-width wow fadeInUp" data-wow-duration=".5s">
                <p>
                    <?= word_limit($profile->description, 100) ?>
                </p>
                <a href="#modal-content" data-toggle="modal-onz" class="btn btn-green mt-5">Selengkapnya</a>
            </div>
        </div>
    </section>

    <section class="content py-100" id="kalkulator">
        <div class="container">
            <div class="content-title text-center wow fadeInUp" data-wow-duration=".5s">
                <h2>Kalkulator Zakat</h2>
            </div>
            <form action="index.php?member=bayar-zakat" id="form-kalkulator-zakat" method="post">
                <input type="hidden" name="type">
                <div class="content-body">
                    <ul class="nav nav-pills mb-3 d-flex justify-content-center nav-tab wow fadeInUp" data-wow-duration=".5s" id="kalkulator-tab"
                        role="tablist">
                        <li class="nav-item">
                            <a class="btn btn-green border-green" data-toggle="pill" type="penghasilan" href="#z-penghasilan">Zakat Penghasilan</a>
                        </li>
                        <li class="nav-item">
                            <a class="btn btn-green-light border-green" data-toggle="pill" type="perdagangan" href="#z-perdagangan">Zakat Perdagangan</a>
                        </li>
                        <li class="nav-item">
                            <a class="btn btn-green-light border-green" data-toggle="pill" type="tabungan" href="#z-tabungan">Zakat Tabungan</a>
                        </li>
                        <li class="nav-item">
                            <a class="btn btn-green-light border-green" data-toggle="pill" type="emas" href="#z-emas">Zakat Emas</a>
                        </li>
                    </ul>
                    <div class="tab-content onz-tab-content" id="pills-tabContent wow fadeInUp" data-wow-duration=".5s">
                        <div class="tab-pane fade show active" id="z-penghasilan" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Penghasilan per Bulan</label>
                                        <input type="number" value="0" min="0" name="z_1_penghasilan" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Pendapatan Lainnya (per Bulan)</label>
                                        <input type="number" value="0" min="0" name="z_1_pendapatan_lain" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Pengeluaran (per Bulan)</label>
                                        <input type="number" value="0" min="0" name="z_1_pengeluaran" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Harga Emas Saat Ini</label>
                                        <input type="number" class="harga-beras form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Nishab (85gr emas)</label>
                                        <input type="number" class="nishab-beras form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Apakah wajib bayar zakat?</label>
                                        <input type="text" name="z_1_wajib_zakat" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jml. Zakat Penghasilan (per Bulan)</label>
                                        <input type="number" value="0" min="0" name="z_1_zakat_bln" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jml. Zakat Penghasilan (per Tahun)</label>
                                        <input type="number" value="0" min="0" name="z_1_zakat_thn" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="z-perdagangan" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Perputaran Modal Selama 1 Tahun</label>
                                        <input type="number" value="0" min="0" name="z_2_modal" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Keuntungan Selama 1 Tahun</label>
                                        <input type="number" value="0" min="0" name="z_2_untung" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Piutang Dagang</label>
                                        <input type="number" value="0" min="0" name="z_2_piutang" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Hutang Jatuh Tempo</label>
                                        <input type="number" value="0" min="0" name="z_2_hutang" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Kerugian Selama 1 Tahun</label>
                                        <input type="number" value="0" min="0" name="z_2_rugi" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Harga Emas Saat Ini</label>
                                        <input type="number" class="harga-emas form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Nishab (85gram)</label>
                                        <input type="number" class="nishab-emas form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Wajib bayar zakat perdagangan?</label>
                                        <input type="text" name="z_2_wajib_zakat" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Jml. Zakat Perdagangan</label>
                                        <input type="number" value="0" min="0" name="z_2_zakat" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="z-tabungan" role="tabpanel">
                            <div class="row">
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Saldo Tabungan</label>
                                        <input type="number" value="0" min="0" name="z_3_saldo" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Bagi Hasil</label>
                                        <input type="number" value="0" min="0" name="z_3_bagi_hasil" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Harga Emas Saat Ini</label>
                                        <input type="number" class="harga-emas form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Nishab (85gram)</label>
                                        <input type="number" class="nishab-emas form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Wajib Bayar Zakat Emas?</label>
                                        <input type="text" name="z_3_wajib_zakat" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label>Jml. Zakat Tabungan</label>
                                        <input type="number" value="0" min="0" name="z_3_zakat" value="625000" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="z-emas" role="tabpanel">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Jml. Emas yg Dimiliki (gram)</label>
                                            <input type="number" value="0" min="0" name="z_4_emas" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Harga Emas Saat Ini</label>
                                            <input type="number" class="harga-emas form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nishab (85gram)</label>
                                            <input type="number" class="nishab-emas form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Wajib Bayar Zakat Emas?</label>
                                            <input type="text" name="z_4_wajib_zakat" class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Jml. Zakat Emas</label>
                                            <input type="number" value="0" min="0" name="z_4_zakat" class="form-control" readonly>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-green mt-4" name="submit">Bayar Zakat</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    
    <form action="login.php" method="post">
    <section id="modal-login" class="onz-modal fadeInDown">
            <div class="container">
                <div class="content my-5">
                    <div class="content-title text-center">
                        <a href="#" data-toggle="modal-onz-close" class="mb-5 btn btn-green-light border-green">
                            <i class="fa fa-close"></i>
                        </a>
                        <h2>Masuk Akun</h2>
                    </div>
                    <div class="content-body d-flex justify-content-center">
                            <div class="login-form">
                                <div class="form-group">
                                    <label>Alamat Email</label>
                                    <input type="email" name="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Kata Sandi</label>
                                    <input type="password" name="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-green" name="submit">
                                        Masuk Akun
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    </form>

    <section id="modal-content" class="onz-modal fadeInDown">
        <div class="container">
            <div class="content my-5">
                <div class="content-title text-center">
                    <a href="#" data-toggle="modal-onz-close" class="mb-5 btn btn-green-light border-green">
                        <i class="fa fa-close"></i>
                    </a>
                    <h2><?= $profile->title ?></h2>
                </div>
                <div class="content-body">
                    <div class="text-center">
                        <img class="rounded w-50 mb-3" src="<?= SITE_URL.'/'.$profile->thumbnail ?>" width="90%" alt="<?= $profile->title ?>">
                    </div>
                    <?= $profile->description ?>
                </div>
            </div>
        </div>
    </section>