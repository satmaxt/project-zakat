<footer class="footer py-4 text-center">
        <span>Copyright &copy; 2018 On:Zakat. All Right Reserved.</span>    
    </footer>

    <a href="#home" id="toTop" class="btn btn-green btn-to-top scroll-link animated" style="visibility: hidden;">
        <i class="fa fa-angle-up"></i>
    </a>

    <script>
        var hargaBeras = <?= updatePrice('beras') ?>;
        var hargaEmas = <?= updatePrice('emas') ?>;
        var nishabBeras = 520;
        var nishabEmas = 85;
        var nishabBerasHarga = nishabBeras*hargaBeras;
        var nishabEmasHarga = nishabEmas*hargaEmas;
    </script>
    <script src="<?= SITE_URL ?>/assets/js/main.min.js"></script>
</body>

</html>