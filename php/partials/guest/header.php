<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= setting('site_name') ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= SITE_URL ?>/assets/css/app.min.css">
</head>

<body>
    <header id="home" class="top-header">
        <section class="top-infobar">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="info-item">
                            <span class="icon">
                                <i class="fa fa-envelope fa-fw"></i>
                            </span>
                            <a href="mailto:info@onzakat.com"><?= setting('email') ?></a>
                        </div>
                        <div class="info-item">
                            <span class="icon">
                                <i class="fa fa-phone fa-fw"></i>
                            </span>
                            <a href="tel:<?= setting('no_telp') ?>"><?= setting('no_telp') ?></a>
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="info-item rtl social">
                            <a href="http://fb.com/<?= setting('social_fb') ?>">
                                <i class="fa fa-facebook fa-fw"></i>
                            </a>
                        </div>
                        <div class="info-item rtl social">
                            <a href="http://twitter.com/<?= setting('social_twitter') ?>">
                                <i class="fa fa-twitter fa-fw"></i>
                            </a>
                        </div>
                        <div class="info-item rtl social">
                            <a href="http://plus.google.com/<?= setting('social_gplus') ?>">
                                <i class="fa fa-google-plus fa-fw"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <nav class="navbar navbar-expand-lg onz-navbar">
            <div class="container">
                <a class="navbar-brand" href="#"><?= setting('site_name') ?></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link scroll-link" href="#home">Beranda</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link scroll-link" href="#tentang-kami">Tentang Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link scroll-link" href="#kalkulator">Kalkulator Zakat</a>
                        </li>
                        <li class="nav-item btn-navbar">
                            <a href="#modal-login" data-toggle="modal-onz" class="nav-link">Masuk Akun</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>