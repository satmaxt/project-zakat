<?php defined('ONZAKAT') or die("No Script Kiddies Please!") ?>

<div class="alert alert-<?= !isset($sess['class']) ? 'default' : $sess['class'] ?>">
	<?php if (isset($sess['title'])): ?>
		<strong><?= $sess['title'] ?></strong><br />
	<?php endif ?>
    <?php if ($sess['message']) {
    	if(is_array($sess['message'])) {
    		foreach($sess['message'] as $row) {
    			echo $row.'<br/>';
    		}
    	} else {
    		echo $sess['message'];
    	}
    } ?>
</div>