<?php defined("ONZAKAT") or die("No Script Kiddies Please!") ?>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	<?= isset($page_title) ? str_replace(' - ', '', $page_title) : '' ?>
	<small><?= isset($page_description) ? $page_description : '' ?></small>
	<?php if ($_GET['member'] == "dashboard"): ?>
		<a href="#welcome" data-toggle="collapse" class="btn btn-sm btn-link">
			<div class="text-success">
				<i class="fa fa-angle-down fa-2x"></i>
			</div>
		</a>
	<?php endif ?>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="<?= SITE_URL ?>/index.php?member=dashboard">
			<i class="fa fa-dashboard fa-fw"></i>Beranda</a>
		</li>

		<?php if (isset($breadcrumbs)) { 
			if(is_array($breadcrumbs)) {
				foreach ($breadcrumbs as $idx => $row):
		?>
				<li><a href="<?= isset($row['url']) ? $row['url'] : 'javascript:void(0)' ?>"><?= $row['val'] ?></a></li>
		<?php
				endforeach;
			}
			
		} ?>
	</ol>
</section>