<?php defined("ONZAKAT") or die("No Script Kiddies Please!") ?>

<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= SITE_URL.'/'.user()->foto ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?= user()->nama ?></p>
                <!-- <a href="#" title="Terdaftar Sejak">
                <i class="fa fa-calendar text-info"></i> 22 Juli 1999</a> -->
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">NAVIGASI MENU</li>
            <li class="<?= ($_GET['member'] == 'dashboard') ? 'active' : '' ?>">
                <a href="index.php?member=dashboard"><i class="fa fa-dashboard"></i> <span>Beranda</span></a>
            </li>
            <li class="<?= ($_GET['member'] == 'kalkulator') ? 'active' : '' ?>">
                <a href="index.php?member=kalkulator"><i class="fa fa-calculator"></i> <span>Kalkulator Zakat</span></a>
            </li>
            <li class="<?= ($_GET['member'] == 'zakat') ? 'active' : '' ?>">
                <a href="index.php?member=zakat"><i class="fa fa-handshake-o"></i> <span>Zakat</span></a>
            </li>
            <li class="<?= ($_GET['member'] == 'pembayaran') ? 'active' : '' ?>">
                <a href="index.php?member=pembayaran"><i class="fa fa-money"></i> <span>Konfirmasi Pembayaran</span></a>
            </li>
            <li class="<?= ($_GET['member'] == 'pertanyaan') ? 'active' : '' ?>">
                <a href="index.php?member=pertanyaan"><i class="fa fa-question-circle"></i> <span>Pertanyaan</span></a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>