<?php defined("ONZAKAT") or die("No Script Kiddies Please!") ?>
</div>
<footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; <?= date('Y') ?>
                <a href="<?= SITE_URL ?>">On:Zakat</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="<?= SITE_URL ?>/assets/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?= SITE_URL ?>/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?= SITE_URL ?>/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck -->
    <script src="<?= SITE_URL ?>/assets/plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="<?= SITE_URL ?>/assets/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= SITE_URL ?>/assets/dist/js/adminlte.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.sidebar-menu').tree()
        });
        $(document).ready(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_minimal-green',
                radioClass: 'iradio_minimal-green'
            });
        });
    </script>
    <?= !isset($footer_scripts) ? '' : $footer_scripts ?>
</body>

</html>