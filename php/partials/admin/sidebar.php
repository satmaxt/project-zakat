<?php defined("ONZAKAT") or die("No Script Kiddies Please!") ?>

<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= SITE_URL.'/'.user()->foto ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?= user()->nama ?></p>
                <!-- <a href="#" title="Terdaftar Sejak">
                <i class="fa fa-calendar text-info"></i> 22 Juli 1999</a> -->
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">ADMIN MENU</li>
            <li class="<?= ($_GET['admin'] == 'dashboard') ? 'active' : '' ?>">
                <a href="index.php?admin=dashboard">
                    <i class="fa fa-dashboard"></i>
                    <span>Beranda</span>
                </a>
            </li>
            <li class="<?= ($_GET['admin'] == 'zakat') ? 'active' : '' ?>">
                <a href="index.php?admin=zakat">
                    <i class="fa fa-handshake-o"></i>
                    <span>Kelola Zakat</span>
                </a>
            </li>
            <li class="<?= ($_GET['admin'] == 'member' || $_GET['admin'] == 'member-edit') ? 'active' : '' ?>">
                <a href="index.php?admin=member">
                    <i class="fa fa-users"></i>
                    <span>Kelola Member</span>
                </a>
            </li>
            <li class="<?= ($_GET['admin'] == 'pertanyaan' || $_GET['admin'] == 'jawab-pertanyaan') ? 'active' : '' ?>">
                <a href="index.php?admin=pertanyaan">
                    <i class="fa fa-question-circle"></i>
                    <span>Kelola Pertanyaan</span>
                </a>
            </li>
            <li class="<?= ($_GET['admin'] == 'rekening') ? 'active' : '' ?>">
                <a href="index.php?admin=rekening">
                    <i class="fa fa-credit-card"></i>
                    <span>Kelola Rekening</span>
                </a>
            </li>
            <li class="<?= ($_GET['admin'] == 'profile') ? 'active' : '' ?>">
                <a href="index.php?admin=profile">
                    <i class="fa fa-user"></i>
                    <span>Halaman Profil</span>
                </a>
            </li>
            <li class="header">MEMBER MENU</li>
            <li class="<?= ($_GET['member'] == 'dashboard') ? 'active' : '' ?>">
                <a href="index.php?member=dashboard"><i class="fa fa-dashboard"></i> <span>Beranda</span></a>
            </li>
            <li class="<?= ($_GET['member'] == 'kalkulator') ? 'active' : '' ?>">
                <a href="index.php?member=kalkulator"><i class="fa fa-calculator"></i> <span>Kalkulator Zakat</span></a>
            </li>
            <li class="<?= ($_GET['member'] == 'zakat') ? 'active' : '' ?>">
                <a href="index.php?member=zakat"><i class="fa fa-handshake-o"></i> <span>Zakat</span></a>
            </li>
            <li class="<?= ($_GET['member'] == 'pembayaran') ? 'active' : '' ?>">
                <a href="index.php?member=pembayaran"><i class="fa fa-money"></i> <span>Konfirmasi Pembayaran</span></a>
            </li>
            <li class="<?= ($_GET['member'] == 'pertanyaan') ? 'active' : '' ?>">
                <a href="index.php?member=pertanyaan"><i class="fa fa-question-circle"></i> <span>Pertanyaan</span></a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>