<?php defined("ONZAKAT") or die("No Script Kiddies Please!") ?>
    <!-- jQuery 3 -->
    <script src="<?= SITE_URL ?>/assets/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?= SITE_URL ?>/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?= SITE_URL ?>/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?= SITE_URL ?>/assets/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= SITE_URL ?>/assets/dist/js/adminlte.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.sidebar-menu').tree()
        })
    </script>
    <?= !isset($footer_scripts) ? '' : $footer_scripts ?>
</body>

</html>