<?php
namespace Data;

use PDO;
use Core\DB;
use Data\UploadFile;

class Auth
{
    protected static $email;
    protected static $password;
    protected static $user;

    public static function login($email, $password)
    {
        self::$email = $email;
        self::$password = $password;    
        
        if(self::$user = self::getUser()) {
            if(self::verifyPassword()) {
                $redirect = self::$user->is_admin ? '?admin=dashboard' : '?member=dashboard';
                $_SESSION['user'] = self::$user;
                header("location: index.php?".$redirect);
            }
        }

        $_SESSION['flash_message'] = [
            'class' => 'warning',
            'title' => 'Error!',
            'message' => ['Pastikan email dan password sudah banar.'],
        ];
        header("location: login.php");
        exit;
    }

    public static function register($data)
    {

        $input = self::validateRegister($data['post'], $data['foto']);
        $inputStr = $input['data'];

        $uploadFile = new UploadFile($input['file']);
        $foto = $uploadFile->storeFile('uploads', 3000000, time());

        if(!$foto) {
            $_SESSION['flash_message'] = [
                "title" => "Error!",
                "class" => "warning",
                "message" => "Gagal mengunggah foto. Maksimal berukuran 3mb",
            ];
            header("location: register.php");
            exit;
        }

        $conn = DB::conn();
        $q = $conn->prepare("INSERT INTO users (email, username, nama, jenis_kelamin, tanggal_lahir, alamat, foto, telp, password) VALUES (?,?,?,?,?,?,?,?,?)");
        $q->execute([
            $inputStr['email'],
            $inputStr['username'],
            $inputStr['nama'],
            $inputStr['jenis_kelamin'],
            date('Y-m-d', strtotime($inputStr['tgl_lahir'])),
            $inputStr['alamat'],
            $foto,
            $inputStr['telp'],
            password_hash($inputStr['password'], PASSWORD_BCRYPT)
        ]);

        $_SESSION['user'] = self::getUser($conn->lastInsertId());

        header("location: index.php?member=member");
    }

    private static function validateRegister($data, $file)
    {
        $error = array();
        $suffix = " tidak boleh kosong";

        foreach($data as $idx => $row) {

            if($idx == "submit") continue;

            if(trim($row) == "") {
                $error[] = ucfirst($idx).$suffix;
            }

            if(trim($row) !== "" && $idx == "email" || $idx == "username") {
                if(self::unique($idx, $row)) {
                    $error[] = ucfirst($idx)." sudah digunakan member lain";
                }
            }

            if($idx == "password" && trim($data['password_confirmation']) !== "") {
                if($row !== $data['password_confirmation']) {
                    $error[] = "Konfirmasi password salah";
                }
            }
        }

        if(!$file) {
            $error[] = "Foto".$suffix;
        } else {
            if(!getimagesize($file['tmp_name'])) {
                $error[] = "File yang diunggah harus berupa foto/gambar.";
            }
        }

        if(count($error) > 0) {
            $_SESSION['flash_message'] = [
                'class' => 'warning',
                'title' => 'Error!',
                'message' => $error,
            ];
            header("location:register.php");
            exit;
        } else {
            return ['data'=>$data, 'file'=>$file];
        }
    }

    private static function unique($type, $value)
    {
        $q = DB::conn()->prepare("SELECT * FROM users WHERE ".$type." = ?");
        $q->execute([
            $value,
        ]);
        return $q->rowCount() > 0 ? true : false;
    }

    private static function verifyPassword()
    {
        return password_verify(self::$password, self::$user->password);
    }

    public static function getUser($id = false)
    {
        $query = "SELECT * FROM users WHERE email = ?";

        if($id) {
            $query = "SELECT * FROM users WHERE id = ?";
        }

        $q = DB::conn()->prepare($query);
        
        if($id) {
            $q->execute([$id]);
        } else {
            $q->execute([
                self::$email
            ]);
        }
        
        return $q->fetch(PDO::FETCH_OBJ);
    }
}