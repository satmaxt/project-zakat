<?php
namespace Data;

use Core\DB;
use Data\Zakat;
use Data\UploadFile;
use Data\AdminRekening;
use Rakit\Validation\Validator;

class BayarZakat
{
	protected static $idxZakat = [
		'penghasilan' => 1,
		'perdagangan' => 2,
		'tabungan' => 3,
		'emas' => 4
	];

	public static function generatePembayaran($request)
	{
		$validator = new Validator();
		$validation = $validator->validate($request, [
			'type' => 'required|in:penghasilan,perdagangan,tabungan,emas'
		]);

		if($validation->fails()) {
			$_SESSION['flash_message'] = [
				'title' => 'Error!',
				'class' => 'warning',
				'message' => 'Jenis zakat tidak ditemukan.'
			];
			return false;
		}

		$rules = array();

		if ($request['type'] == "penghasilan") {
			$rules = array(
				'z_1_wajib_zakat' => 'required|in:YA,TIDAK',
				'z_1_zakat_bln' => 'required|numeric',
				'z_1_zakat_thn' => 'required|numeric',
			);
		} else {
			$rules = array(
				'z_'.self::$idxZakat[$request['type']].'_wajib_zakat' => 'required|in:YA,TIDAK',
				'z_'.self::$idxZakat[$request['type']].'_zakat' => 'required|numeric',
			);
		}

		$validation = $validator->validate($request, $rules);

		if($validation->fails()) {
			$errors = $validation->errors();

			$_SESSION['flash_message'] = [
				'title' => 'Error!',
				'class' => 'warning',
				'message' => $errors->firstOfAll()
			];
			return false;
		}

		if($request['type'] == "penghasilan")  {
			return [
				'type' => $request['type'],
				'zakat_bln' => ceil($request['z_1_zakat_bln']),
				'zakat_thn' => ceil($request['z_1_zakat_thn']),
				'wajib_zakat' => self::wajibZakat($request['z_1_wajib_zakat']),
			];
		} else {
			return [
				'type' => $request['type'],
				'besar_zakat' => ceil($request['z_'.self::$idxZakat[$request['type']].'_zakat']),
				'wajib_zakat' => self::wajibZakat($request['z_'.self::$idxZakat[$request['type']].'_wajib_zakat']),
			];
		}
	}

	public static function storePembayaran($request)
	{
		$rules = [];
		$validator = new Validator;
		$validation = $validator->validate($request, [
			'jenis_zakat' => 'required|in:penghasilan,perdagangan,tabungan,emas',
			'besar_zakat' => 'required|numeric',
			'metode_pembayaran' => 'required',
		]);

		if(!self::isValid($validation)) return false;

		if($request['jenis_zakat'] == "penghasilan"):

			$validation = $validator->validate($request, [
				'pembayaran' => 'required|in:thn,bln'
			]);

			if(!self::isValid($validation)) return false;

		endif;

		$metodePembayaran = AdminRekening::getDataByType($request['metode_pembayaran']);

		if(!$metodePembayaran) {
			$_SESSION['flash_message'] = [
				'title' => 'Error!',
				'class' => 'warning',
				'message' => 'Metode pembayaran tidak valid',
			];
			return false;
		}

		$db = DB::conn();
		$pembayaran = self::pembayaran($request);

		$q = $db->prepare('INSERT INTO zakat (user_id, jenis_zakat, jumlah, rekening_id, monthly, status) VALUES (?,?,?,?,?,?)');
		$q->bindParam(1, user()->id);
		$q->bindParam(2, $request['jenis_zakat']);
		$q->bindParam(3, $request['besar_zakat']);
		$q->bindParam(4, $metodePembayaran->id);
		$q->bindParam(5, $pembayaran);
		$q->bindValue(6, 'unpaid');
		
		if($q->execute()) {
			return $db->lastInsertId();
		}
		$_SESSION['flash_message'] = [
			'title' => 'Error!',
			'class' => 'warning',
			'message' => 'Gagal melakukan pembayaran',
		];

		return false;
	}

	public static function wajibZakat($str)
	{
		return $str == "YA" ? true : false;
	}

	public static function pembayaran($request)
	{
		if(isset($request['pembayaran'])) {
			return $request['pembayaran'] == "bln" ? true : false;
		}
		return null;
	}

	public static function isValid($validation)
	{
		if($validation->fails()) {
			$errors = $validation->errors();
			$_SESSION['flash_message'] = [
				'title' => 'Error!',
				'class' => 'warning',
				'message' => $errors->firstOfAll(),
			];
			return false;
		}

		return true;
	}

	public static function confirmPembayaran($id, $request, $user_id = false)
	{
		$message = [];
		$notErr = true;
		$validator = new Validator;
		$db = DB::conn();
		$validation = $validator->validate($request['post']+$request['files'], [
			'thumbnail' => 'uploaded_file:10,3000k,png,jpeg',
		]);

		$post = $request['post'];
		$files = $request['files'];

		if($validation->fails()) {
			$errors = $validation->errors();
			$_SESSION['flash_message'] = [
				'title' => 'Error!',
				'class' => 'warning',
				'message' => $errors->firstOfAll()
			];
			return false;
		}

		if(isset($post['metode_pembayaran'])) {
			if(trim($post['metode_pembayaran']) !== "") {
				$metodePembayaran = AdminRekening::getDataByType($post['metode_pembayaran']);

				if(!$metodePembayaran) {
					$_SESSION['flash_message'] = [
						'title' => 'Error!',
						'class' => 'warning',
						'message' => 'Metode pembayaran salah',
					];
					return false;
				}

				$query = "UPDATE zakat SET rekening_id = ? WHERE id = ?";
				if($user_id) $query .= " AND user_id = ?";
				$q = $db->prepare("UPDATE zakat SET rekening_id = ? WHERE id = ?");
				$q->bindParam(1, $metodePembayaran->id);
				$q->bindParam(2, $id);
				if($user_id) $q->bindParam(3, $user_id);
				$notErr = $q->execute();
				$message[] = $notErr ? "Berhasil mengganti metode pembayaran" : "Gagal mengganti metode pembayaran";
			}
		}

		if(isset($files['bukti_pembayaran'])) {
			if(file_exists($files['bukti_pembayaran']['tmp_name'])) {
				$uploadFile = new UploadFile($files['bukti_pembayaran']);
				$buktiPembayaran = $uploadFile->storeFile('uploads/bukti', 3000000, time());
				$query = "UPDATE zakat SET bukti_foto = ?, status = ? WHERE id = ?";
				if($user_id) $query .= " AND user_id = ?";
				$q = $db->prepare($query);
				$q->bindParam(1, $buktiPembayaran);
				$q->bindValue(2, 'waiting');
				$q->bindParam(3, $id);
				if($user_id) $q->bindParam(4, $user_id);
				$notErr = $q->execute();
				$message[] = $notErr ? "Berhasil mengunggah bukti pembayaran" : "Gagal mengunggah bukti pembayaran";
			}
		}
		$_SESSION['flash_message'] = [
			"title" => "Info",
			"class" => "info",
			"message" => $message
		];
		return $notErr;
	}
}
