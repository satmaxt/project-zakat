<?php
namespace Data;

use PDO;
use Core\DB;
use Rakit\Validation\Validator;

class AdminRekening
{
	public static function getData($withTrashed = false, $id = false)
	{
		$db = DB::conn();
		$query = "SELECT * FROM rekening WHERE deleted = ?";

		if($id) {
			$query .= " AND id = ?";
		}

		$q = $db->prepare($query);
		$q->bindParam(1, $withTrashed);

		if($id) {
			$q->bindParam(2, $id);
		}

		$q->execute();

		return $q;
	}

	public static function storeData($request)
	{
		$validator = new Validator;
		$validation = $validator->validate($request, [
			'nama' => 'required|min:2|max:191',
			'jenis' => 'required|alpha_num|min:3|max:20',
			'pemilik' => 'required|min:4|max:191',
			'nomor' => 'required|min:4|max:50',
		]);

		if($validation->fails()) {
			$errors = $validation->errors();
			$_SESSION['flash_message'] = [
				'title' => 'Error!',
				'class' => 'warning',
				'message' => $errors->firstOfAll()
			];
			return false;
		}

		$db = DB::conn();
		$q = $db->prepare("INSERT INTO rekening (jenis, nama, pemilik, nomor) VALUES (?,?,?,?)");
		return $q->execute([
			strtolower($request['jenis']),
			$request['nama'],
			$request['pemilik'],
			$request['nomor'],
		]);
	}

	public static function updateData($request, $id)
	{
		$validator = new Validator;
		$validation = $validator->validate($request, [
			'nama' => 'required|min:2|max:191',
			'jenis' => 'required|alpha_num|min:3|max:20',
			'pemilik' => 'required|min:4|max:191',
			'nomor' => 'required|min:4|max:50',
		]);

		if($validation->fails()) {
			$errors = $validation->errors();
			$_SESSION['flash_message'] = [
				'title' => 'Error!',
				'class' => 'warning',
				'message' => $errors->firstOfAll()
			];
			return false;
		}

		$db = DB::conn();
		$q = $db->prepare("UPDATE rekening SET jenis = ?, nama = ?, pemilik = ?, nomor = ? WHERE id = ?");
		return $q->execute([
			strtolower($request['jenis']),
			$request['nama'],
			$request['pemilik'],
			$request['nomor'],
			$id
		]);
	}

	public static function deleteData($id)
	{
		$db = DB::conn();
		$q = $db->prepare("UPDATE rekening SET deleted = 1 WHERE id = ?");
		return $q->execute([
			$id
		]);
	}

	public static function getDataByType($type)
	{
		$db = DB::conn()->prepare("SELECT * FROM rekening WHERE jenis = ? AND deleted = 0");
		$db->execute([$type]);
		return $db->fetch(PDO::FETCH_OBJ);
	}
}