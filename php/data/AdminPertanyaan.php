<?php 
namespace Data;

use PDO;
use Core\DB;
use Rakit\Validation\Validator;

class AdminPertanyaan
{
	public static function delete($id, $user_id = false)
	{
		$db = DB::conn();
		$query = $db->prepare("DELETE FROM pertanyaan WHERE id = ? AND user_id = ?");
		$query->execute([$id, $user_id]);
		return $query;
	}

	public static function getData($user_id = false)
	{
		$db = DB::conn();
		$exec = [];
		$q = "SELECT A.id AS pertanyaan_id, A.user_id, A.judul, A.detail, A.jawaban, A.pada, B.nama, B.username, B.email FROM pertanyaan AS A INNER JOIN users AS B ON B.id = A.user_id";

		if($user_id) {
			$q .= " WHERE A.user_id = ?";
			$exec[] = $user_id;
		}

		$query = $db->prepare($q);
		$query->execute($exec);
		return $query;
	}

	public static function getPertanyaan($id, $user_id = false)
	{
		$db = DB::conn($id);
		$q = "SELECT A.id AS pertanyaan_id, A.user_id, A.judul, A.detail, A.jawaban, A.pada, B.nama, B.username, B.email FROM pertanyaan AS A INNER JOIN users AS B ON B.id = A.user_id WHERE A.id = ?";

		$exec = [$id];

		if($user_id) {
			$q .= " AND A.user_id = ?";
			$exec[] = $user_id;
		}

		$query = $db->prepare($q);
		$query->execute($exec);
		return $query->fetch(PDO::FETCH_OBJ);
	}

	public static function update($id, $jawaban, $user_id = false)
	{
		$db = DB::conn($id);
		$q = "UPDATE pertanyaan SET jawaban = ? WHERE id = ?";

		$exec = [$jawaban, $id];

		if($user_id) {
			$q .= " AND user_id = ?";
			$exec[] = $user_id;
		}

		$query = $db->prepare($q);
		return $query->execute($exec);
	}

	public static function storeData($user_id, $request)
	{
		$validator = new Validator;
		$validation = $validator->validate($request, [
			'judul' => 'required|min:3',
			'detail' => 'required|min:10',
		]);

		if($validation->fails()) {
			$errors = $validation->errors();
			$_SESSION['flash_message'] = [
				'title'	 => 'Error!',
				'class' => 'warning',
				'message' => $errors->firstOfAll()
			];
		}

		$db = DB::conn();
		$q = $db->prepare("INSERT INTO pertanyaan (user_id, judul, detail) VALUES (?, ?, ?)");
		return $q->execute([
			$user_id, $request['judul'], $request['detail']
		]);
	}
}
