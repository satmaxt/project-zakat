<?php
namespace Data;

use PDO;
use Core\DB;

class Zakat
{
	public static function getData($id = false, $user_id = false, $status = false)
	{
		$db = DB::conn();
		$where = [];

		if($id) {
			$where[] = "A.id = :id";
		}

		if($user_id) {
			$where[] = 'A.user_id = :user_id';
		}

		if($status) {
			$where[] = $status;
		}
		
		$where = (count($where) > 0) ? "WHERE (".implode(' AND ', $where).")" : "";
		$query = "SELECT A.*, C.nama AS user_nama, C.email AS user_email, C.username AS user_username, B.nama AS nama_bank, B.jenis AS jenis_bank, B.pemilik AS pemilik_rekening, B.nomor AS nomor_rekening FROM zakat AS A INNER JOIN rekening AS B ON B.id = A.rekening_id INNER JOIN users AS C ON C.id = A.user_id ".$where." ORDER BY A.id DESC";
		$q = $db->prepare($query);
		if($id) {
			$q->bindParam('id', $id);
		}
		if($user_id) {
			$q->bindParam('user_id', $user_id);
		}

		$q->execute();

		return $q;
	}

	public static function delete($id)
	{
		$db = DB::conn();
		$q = $db->prepare("DELETE FROM zakat WHERE id = ?");
		return $q->execute([$id]);
	}

	public static function konfirmasi($id, $methode)
	{
		$old = self::getData($id)->fetch(PDO::FETCH_OBJ);
		$return = $old->bukti_foto ? 'waiting' : 'unpaid';
		$methode = $methode == "approve" ? 'success' : $return;
		$db = DB::conn();
		$q = $db->prepare("UPDATE zakat SET `status` = ? WHERE id = ?");
		return $q->execute([
			$methode, $id
		]);
	}
}