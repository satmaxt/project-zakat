<?php
namespace Data;

use PDO;
use Core\DB;
use Rakit\Validation\Validator;

class Page
{
	public static function getPage($pageType, $id = false, $slug = false)
	{
		$db = DB::conn();
		$query = '';

		if($id && $slug) {
			$query = 'id = :id, slug = :slug, ';
		}

		$q = $db->prepare("SELECT * FROM pages WHERE (".$query."page_type = :page_type)");

		if($id && $slug) {
			$q->bindParam('id', $id);
			$q->bindParam('slug', $slug);
		}

		$q->bindParam('page_type', $pageType);

		$q->execute();

		return $q;
	}

	public static function updatePage($request, $pageType, $id = false, $slug = false)
	{
		$validator = new Validator;
		$validation = $validator->validate($request['post']+$request['files'], [
			'title' => 'required|min:2|max:191',
			'thumbnail' => 'uploaded_file:10,3000k,png,jpeg',
			'description' => 'required|min:10',
		]);

		if($validation->fails()) {
			$errors = $validation->errors();
			$_SESSION['flash_message'] = [
				'title' => 'Error!',
				'class' => 'warning',
				'message' => $errors->firstOfAll()
			];
			return false;
		}

		$db = DB::conn();
		$query = "UPDATE pages SET title = :title, description = :description";

		if(file_exists($request['files']['thumbnail']['tmp_name'])) {
			$uploadFile = new UploadFile($request['files']['thumbnail']);
			$thumbnail = $uploadFile->storeFile('uploads/pages', 3000000, time());
			$query .= ", thumbnail = :thumbnail";
		}

		if($id && $slug) {
			$query .= " WHERE (id = :id, slug = :slug, page_type = :page_type)";
		} else {
			$query .= " WHERE page_type = :page_type";
		}

		$q = $db->prepare($query);
		$q->bindParam('title', $request['post']['title']);
		$q->bindParam('description', $request['post']['description']);
		$q->bindParam('page_type', $pageType);

		if(isset($thumbnail)) {
			$q->bindParam('thumbnail', $thumbnail);
		}

		if($id && $slug) {
			$q->bindParam('id', $id);
			$q->bindParam('slug', $slug);
		}


		return $q->execute();
	}

	public static function slug($title, $separator = "-")
	{
		// Convert all dashes/underscores into separator
        $flip = $separator == '-' ? '_' : '-';
        $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);
        // Replace @ with the word 'at'
        $title = str_replace('@', $separator.'at'.$separator, $title);
        // Remove all characters that are not the separator, letters, numbers, or whitespace.
        $title = preg_replace('![^'.preg_quote($separator).'\pL\pN\s]+!u', '', mb_strtolower($title));
        // Replace all separator characters and whitespace by a single separator
        $title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);
        return trim($title, $separator);
	}
}