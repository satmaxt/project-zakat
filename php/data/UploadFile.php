<?php

namespace Data;

class UploadFile
{
	private $file;

	public function __construct($file)
	{
		$this->file = $file;	
	}

	public function setFile($file)
	{
		$this->file = $file;
	}

	public function getFileInfo()
	{
		$pathInfo = pathinfo($this->file['name']);

		$data = (object)array(
			"baseName" => $pathInfo['basename'],
			"fileExtension" => $pathInfo['extension'],
			"fileName" => $pathInfo['filename'],
			"fileSize" => $this->file['size'],
			"mimeType" => $this->file['type'],
			"fileTempName" => $this->file['tmp_name'],
		);

		return $data;
	}

	public function storeFile($uploadPath, $maxSize = false, $newName = false)
	{

		$fileInfo = $this->getFileInfo();

		if(!file_exists($uploadPath)) {
			return false;
		}
		
		if($newName) {
			$fileName = $uploadPath."/".$newName.".".$fileInfo->fileExtension;
		} else {
			$fileName = $uploadPath."/".$fileInfo->baseName;
		}

		if($fileInfo->fileSize >= $maxSize) {
			return false;
		}

		if(move_uploaded_file($fileInfo->fileTempName, $fileName)) {
			return $fileName;
		}

		return false;
	}
}