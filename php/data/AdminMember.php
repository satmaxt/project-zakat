<?php
namespace Data;

use PDO;
use Core\DB;
use Data\UploadFile;
use Rakit\Validation\Validator;

class AdminMember
{
	/**
	 * Get member data
	 * 
	 * @param int optional $id
	 * @return object
	 */
    public static function getMember($id = false)
    {
    	$db = DB::conn();
    	if(!$id) {
    		return $db->query("SELECT * FROM users", PDO::FETCH_OBJ)->fetchAll();
    	} else {
    		$q = $db->prepare("SELECT * FROM users WHERE id = ?");
    		$q->execute([$id]);
    		return $q->fetch(PDO::FETCH_OBJ);
    	}
    }

	/**
	 * Delete member data from users table
	 * 
	 * @param int $id
	 * @return boolean
	 */
    public static function delete($id)
    {
    	$db = DB::conn();
    	$q = $db->prepare("DELETE FROM users WHERE id = ?");
    	$q->execute([$id]);
    	return $q;
    }

	/**
	 * Update member data from users table by id
	 * 
	 * @param array $post
	 * @param array $files
	 * @param int	$id
	 * @return boolean
	 */
    public static function updateMember($post, $file, $id)
    {
		//validate $_POST and $_FILES
		$input = self::validate($post, $file);
		if(!$input) return false;
		
		// Start db connection
		$db = DB::conn();
		
		// building query
		$set = "nama = :nama, email = :email, jenis_kelamin = :jenis_kelamin, tanggal_lahir = :tanggal_lahir, alamat = :alamat, telp = :telp, username = :username, is_admin = :is_admin";
		
		// building param
    	$param = array(
    		"nama" => $input["data"]["nama"],
    		"email" => $input["data"]["email"],
    		"jenis_kelamin" => $input["data"]["jenis_kelamin"],
    		"tanggal_lahir" => $input["data"]["tgl_lahir"],
    		"alamat" => $input["data"]["alamat"],
    		"telp" => $input["data"]["telp"],
    		"username" => $input["data"]["username"],
    		"is_admin" => $input["data"]["is_admin"],
    		"id" => $id,
    	);

		// detect if foto file are exist
    	if(file_exists($input['foto']['tmp_name'])) {
			$set .= ", foto = :foto";
			
			// upload foto
			$uploadFile = new UploadFile($input['foto']);
			
			// store to param
    		$param["foto"] = $uploadFile->storeFile('uploads', 3000000, time());
    	}

		// detect if password are filled
		// if filled, the system will store the hashed password to param
    	if(isset($input['data']['password'])) {
    		$set .= ", password = :password";
    		$param["password"] = password_hash($input["data"]["password"], PASSWORD_BCRYPT);
    	}

		// executing query
		$user = $_SESSION['user'];
		$q = $db->prepare("UPDATE users SET ".$set. " WHERE id = :id");
    	if($q->execute($param)) {
			if($id == $user->id) {
				user($input["data"]["username"]);
			}
			return true;
		}
		return false;
    }

	/**
	 * Validate $_POST and $_FILES
	 * 
	 * @param array $post
	 * @param array $file
	 * @return void
	 */
    public static function validate($post, $file)
    {
    	$validator = new Validator;
    	$validation = $validator->validate($post+$file, [
    		'nama' => 'required|min:3|max:191',
    		'email' => 'required|email',
    		'jenis_kelamin' => 'required|in:L,P',
    		'tgl_lahir' => 'required|date',
    		'foto' => 'uploaded_file:10,3000k,png,jpeg',
    		'alamat' => 'required|min:10',
    		'username' => 'required|min:6|max:16',
    		'telp' => 'required|min:3|max:20',
    		'is_admin' => 'required|in:1,0',
    	]);

    	if($validation->fails()) {
    		return self::validateData($validation);
    	}

    	if(trim($post['password']) !== "") {
    		$validation = $validator->validate($post, [
    			'password' => 'required|min:6|max:16',
    			'password_confirmation' => 'required|same:password'
    		]);
    		if($validation->fails()) {
    			return self::validateData($validation);
    		}
    	} else {
    		unset($post['password']);
    	}

    	return ["data"=>$post,"foto"=>$file['foto']];
    }

	/**
	 * Get error message for validator
	 * 
	 * @param object $validation
	 * @return bool
	 */
    public static function validateData($validation)
    {
    	$errors = $validation->errors();
		$_SESSION['flash_message'] = [
			'title' => 'Error!',
			'class' => 'warning',
			'message' => $errors->firstOfAll()
		];
		return false;
    }
}
