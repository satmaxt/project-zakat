<?php
$page_title = "Daftar Akun - ";

require_once __DIR__.'/core/init.php';
require_once __DIR__.'/data/UploadFile.php';
require_once __DIR__.'/data/Auth.php';
require_once __DIR__.'/partials/admin/header-login.php';

redirectUnAuth();

if(isset($_POST['submit'])) {
    Data\Auth::register(['post'=>$_POST,'foto'=>$_FILES['foto']]);
}

?>
<body class="hold-transition login-page" >
    <div class="login-box" style="min-width: 30%">
        <div class="login-logo">
            <a href="assets/index2.html"><b>On:</b>Zakat</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Daftarkan diri anda dan mulai berzakat.</p>
            <?php alert(); ?>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" class="form-control">
                </div>
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control">
                </div>
                <div class="form-group">
                    <label>Ulang Password</label>
                    <input type="password" name="password_confirmation" class="form-control">
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <select name="jenis_kelamin" class="form-control">
                        <option value selected>Pilih</option>
                        <option value="L">Laki-laki</option>
                        <option value="P">Perempuan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="date" name="tgl_lahir" class="form-control">
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea name="alamat" rows="3" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label>No. Telp</label>
                    <input type="text" name="telp" class="form-control">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control">
                </div>
                <div class="form-group">
                    <label>Foto</label>
                    <input type="file" name="foto" class="form-control">
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <a href="login.php" class="text-center text-success">Sudah punya akun?</a>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" name="submit" class="btn btn-success btn-block btn-flat">Daftar</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    <?php
        require_once __DIR__.'/partials/admin/footer-login.php';
    ?>