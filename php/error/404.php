<?php
$page_title = "404 Halaman tidak ditemukan - ";

require_once __DIR__.'/../partials/admin/header-login.php';

?>
<body class="hold-transition login-page">
<div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Oops! Halaman tidak ditemukan.</h3>

          <p>
            kami tidak dapat menemukan halaman yang anda cari
            Atau anda dapat <a href="index.php?admin=dashboard">kembali ke dashboard</a>
          </p>
        </div>
        <!-- /.error-content -->
      </div>
<?php
require_once __DIR__.'/../partials/admin/footer-login.php';
?>