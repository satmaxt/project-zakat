<?php

require_once 'core/init.php';

if(isset($_GET['admin']) && !isset($_GET['page'])) {
	switch ($_GET['admin']) {
		case 'dashboard':
			require_once 'admin/index.php';
			break;

		case 'pertanyaan':
			require_once 'admin/pertanyaan.php';
			break;

		case 'jawab-pertanyaan':
			require_once 'admin/pertanyaan-jawab.php';
			break;

		case 'member':
			require_once 'admin/member.php';
			break;

		case 'member-edit':
			require_once 'admin/member-edit.php';
			break;

		case 'profile':
			require_once 'admin/profile.php';
			break;

		case 'rekening':
			require_once 'admin/rekening.php';
			break;

		case 'rekening-edit':
			require_once 'admin/rekening-edit.php';
			break;
		
		case 'zakat':
			require_once 'admin/zakat.php';
			break;
		
		case 'konfirmasi-pembayaran':
			require_once 'admin/zakat.php';

		default:
			require_once 'error/404.php';
			break;
	}
}

if(isset($_GET['member']) && !isset($_GET['page']) && !isset($_GET['admin'])) {
	switch ($_GET['member']) {
		case 'dashboard':
			require_once 'member/index.php';
			break;

		case 'kalkulator':
			require_once 'member/kalkulator.php';
			break;

		case 'bayar-zakat':
			require_once 'member/bayar-zakat.php';
			break;

		case 'konfirmasi-pembayaran':
			require_once 'member/konfirmasi-pembayaran.php';
			break;

		case 'pembayaran':
			require_once 'member/pembayaran.php';
			break;

		case 'zakat':
			require_once 'member/zakat.php';
			break;
			
		case 'pertanyaan':
			require_once 'member/pertanyaan.php';
			break;
		
		default:
			require_once 'error/404.php';
			break;
	}
}

if(!isset($_GET['member']) && !isset($_GET['admin'])) {
	$profile = Data\Page::getPage('profile')->fetch(PDO::FETCH_OBJ);
	require_once 'partials/guest/header.php';
	
	require_once 'partials/guest/content.php';

	require_once 'partials/guest/footer.php';
}

ob_flush();