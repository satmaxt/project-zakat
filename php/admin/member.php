<?php

defined('ONZAKAT') or die ('No Script Kiddies Please!');

if(isset($_SESSION['user'])) {
	if(!(bool)$_SESSION['user']->is_admin) header("location: ?member=dashboard");
} else {
	header("location: login.php");
}

$page_title = "Kelola Member - ";
$page_description = "Kelola member yang terdaftar.";

require_once __DIR__.'/../partials/admin/header.php';

$breadcrumbs = [
	["val"=>"<i class=\"fa fa-users\"></i> Kelola Member"],
];
$users = Data\AdminMember::getMember();

if(isset($_POST['delete'])) {
	if(Data\AdminMember::delete($_POST['id'])) {
		$_SESSION['flash_message'] = [
			'title' => 'Sukses!',
			'class' => 'success',
			'message' => 'Berhasil menghapus member'
		];
		header("location: index.php?admin=member");
		exit;
	}
}

require_once __DIR__.'/../partials/admin/breadcrumbs.php';

?>
<!-- Main content -->
<section class="content">
	<?php alert(); ?>
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Kelola Member</h3>
			<div class="box-tools pull-right"></div>
		</div>
		<div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Kelamin</th>
                            <th>Tgl. Lahir</th>
                            <th>Alamat</th>
                            <th>Telp</th>
                            <th>Foto</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php
                    	$num = 1;
                    	foreach ($users as $user): ?>
                        <tr>
                            <td><?= $num++ ?></td>
                            <td><?= $user->nama ?></td>
                            <td><?= $user->jenis_kelamin == "L" ? 'Laki-laki' : 'Perempuan' ?></td>
                            <td><?= date('d M Y', strtotime($user->tanggal_lahir)) ?></td>
                            <td><?= $user->alamat ?></td>
                            <td><?= $user->telp ?></td>
                            <td>
                                <a href="<?= SITE_URL.'/'.$user->foto ?>" class="thumbnail">
                                    <img width="100px" src="<?= SITE_URL.'/'.$user->foto ?>" alt="bukti">
                                </a>
                            </td>
                            <td>
                                <a href="index.php?admin=member-edit&id=<?= $user->id ?>" class="btn btn-success btn-sm">Edit</a>
                                <form action="index.php?admin=member" style="display: inline-block;" method="post">
									<input type="hidden" name="delete">
									<input type="hidden" name="id" value="<?= $user->id ?>">
									<button class="btn btn-sm btn-danger" name="submit">Hapus</button>
								</form>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</section>
<!-- /.content -->
<?php

require_once __DIR__.'/../partials/admin/footer.php';

?>