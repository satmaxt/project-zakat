<?php

defined('ONZAKAT') or die ('No Script Kiddies Please!');

if(isset($_SESSION['user'])) {
	if(!(bool)$_SESSION['user']->is_admin) header("location: ?member=dashboard");
} else {
	header("location: login.php");
}
if(!isset($_GET['id'])) header("location: index.php?admin=member");

if(isset($_POST['submit'])) {
   $result = Data\AdminMember::updateMember($_POST, $_FILES, $_GET['id']);
   if($result) {
        $_SESSION['flash_message'] = [
            'title' => 'Sukses!',
            'class' => 'success',
            'message' => 'Berhasil menyimpan data member.'
        ];
   }
   header("Refresh:0");
   exit;
}


$page_title = "Edit Member - ";
$page_description = "Edit member yang terdaftar.";

require_once __DIR__.'/../partials/admin/header.php';

$breadcrumbs = [
	["val"=>"<i class=\"fa fa-users\"></i> Kelola Member"],
];
$user = Data\AdminMember::getMember($_GET['id']);

require_once __DIR__.'/../partials/admin/breadcrumbs.php';

?>
<!-- Main content -->
<section class="content">
    <?php alert() ?>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Member</h3>
                    <div class="box-tools pull-right">
                        <a href="index.php?admin=member" class="btn btn-sm btn-success">
                            <i class="fa fa-angle-left"></i>Kembali</a>
                    </div>
                </div>
                <div class="box-body">
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama" value="<?= $user->nama ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <select name="jenis_kelamin" class="form-control">
                                <option value>Pilih</option>
                                <option value="L" <?= $user->jenis_kelamin == "L" ? 'selected' : '' ?>>Laki-laki</option>
                                <option value="P" <?= $user->jenis_kelamin == "P" ? 'selected' : '' ?>>Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="date" name="tgl_lahir" value="<?= $user->tanggal_lahir ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="alamat" rows="3" class="form-control"><?= $user->alamat ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>No. Telp</label>
                            <input type="text" name="telp" value="<?= $user->telp ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" value="<?= $user->email ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Foto</label>
                            <input type="file" name="foto" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" value="<?= $user->username ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Hak Akses</label>
                            <select name="is_admin" class="form-control">
                                <option value>Pilih hak akses</option>
                                <option value="1" <?= $user->is_admin ? 'selected' : '' ?>>Administrator</option>
                                <option value="0" <?= $user->is_admin ? '' : 'selected' ?>>Member</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Ulang Password</label>
                            <input type="password" name="password_confirmation" class="form-control">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success" name="submit">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
<?php

require_once __DIR__.'/../partials/admin/footer.php';

?>