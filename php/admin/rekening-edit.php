<?php

defined('ONZAKAT') or die ('No Script Kiddies Please!');

if(isset($_SESSION['user'])) {
	if(!(bool)$_SESSION['user']->is_admin) header("location: ?member=dashboard");
} else {
	header("location: login.php");
}

if(!isset($_GET['id'])) header("location: index.php?admin=rekening");

$page_title = "Edit Rekening - ";
$page_description = "Ubah data rekening yang telah ditambahkan.";

require_once __DIR__.'/../partials/admin/header.php';

$breadcrumbs = [
	["val"=>"<i class=\"fa fa-credit-card\"></i> Kelola Rekening"],
];
$rekening = Data\AdminRekening::getData(false, $_GET['id'])->fetch(PDO::FETCH_OBJ);

if(isset($_POST['submit'])) {
	$res = Data\AdminRekening::updateData($_POST, $_GET['id']);
	if($res) {
		$_SESSION['flash_message'] = [
			'title' => 'Sukses!',
			'class' => 'success',
			'message' => 'Berhasil menyimpan rekening',
		];
	}
	header("Refresh:0");
	exit;
}

require_once __DIR__.'/../partials/admin/breadcrumbs.php';

?>
<!-- Main content -->
<section class="content">
	<?php alert(); ?>
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Edit Rekening</h3>
			<div class="box-tools pull-right">
				<a href="index.php?admin=rekening" class="btn btn-sm btn-success"><i class="fa fa-angle-left fa-fw"></i>Kembali</a>
			</div>
		</div>
		<div class="box-body">
			<div id="add-rekening">
				<form action="" method="post">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Jenis Bank</label>
								<input type="text" name="jenis" placeholder="Contoh: bca" value="<?= $rekening->jenis ?>" class="form-control">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Nama Bank</label>
								<input type="text" name="nama" placeholder="Contoh: Bank Central Asia" value="<?= $rekening->nama ?>" class="form-control">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Atas Nama</label>
								<input type="text" value="<?= $rekening->pemilik ?>" name="pemilik" placeholder="Contoh: Hermanito" class="form-control">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Nomor Rekening</label>
								<input type="text" value="<?= $rekening->nomor ?>" name="nomor" placeholder="Contoh: 8823758273" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<button class="btn btn-success" name="submit">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
<?php

require_once __DIR__.'/../partials/admin/footer.php';

?>