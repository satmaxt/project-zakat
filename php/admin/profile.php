<?php

defined('ONZAKAT') or die ('No Script Kiddies Please!');

if(isset($_SESSION['user'])) {
	if(!(bool)$_SESSION['user']->is_admin) header("location: ?member=dashboard");
} else {
	header("location: login.php");
}

$page_title = "Halaman Profil - ";
$page_description = "Kelola halaman profil.";

require_once __DIR__.'/../partials/admin/header.php';

$breadcrumbs = [
	["val"=>"<i class=\"fa fa-user\"></i> Halaman Profil"],
];

$page = Data\Page::getPage('profile')->fetch(PDO::FETCH_OBJ);

if(isset($_POST['submit'])) {
	$res = Data\Page::updatePage([
		'post' => $_POST,
		'files' => $_FILES
	], 'profile');

	if($res) {
		$_SESSION['flash_message'] = [
			'title' => 'Sukses!',
			'class' => 'success',
			'message' => 'Berhasil menyimpan profil'
		];
		header("Refresh:0");
		exit;
	}
}


require_once __DIR__.'/../partials/admin/breadcrumbs.php';

?>
<!-- Main content -->
<section class="content">
	<?php alert(); ?>
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Halaman Profil</h3>
			<div class="box-tools pull-right">
				
			</div>
		</div>
		<div class="box-body">
			<form action="" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label>Judul</label>
					<input type="text" name="title" class="form-control" value="<?= $page->title ?>">
				</div>
				<div class="form-group">
					<label>Thumbnail</label>
					<input type="file" name="thumbnail" class="form-control">
				</div>
				<div class="form-group">
					<label>Deskripsi</label>
					<textarea rows="3" name="description" class="form-control"><?= $page->description ?></textarea>
				</div>
				<div class="form-group">
					<button class="btn btn-success" name="submit">Simpan Halaman</button>
				</div>
			</form>
		</div>
	</div>
</section>
<!-- /.content -->
<?php

$footer_scripts = "
	<script src=\"".SITE_URL."/assets/plugins/tinymce/tinymce.min.js\"></script>
	<script>
		tinymce.init({
			selector: 'textarea',
			placeholder: 'Deskripsi halaman disini...',
			height: '300'
		});
	</script>
";

require_once __DIR__.'/../partials/admin/footer.php';

?>