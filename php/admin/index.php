<?php

defined('ONZAKAT') or die ('No Script Kiddies Please!');

if(isset($_SESSION['user'])) {
	if(!(bool)$_SESSION['user']->is_admin) header("location: ?member=dashboard");
} else {
	header("location: login.php");
}

$page_title = "Dashboard - ";
$data = adminDashboardData();

require_once __DIR__.'/../partials/admin/header.php';


require_once __DIR__.'/../partials/admin/breadcrumbs.php';

?>
<!-- Main content -->
<section class="content">
	<div class="panel collapse" id="welcome">
		<div class="panel-body ">
			Selamat datang di area dashboard anda. Di sini anda dapat mengelola pembayaran zakat, melihat riwayat pembayaran zakat, dan mengelola data lainnya.
		</div>
	</div>
	<div class="row">
		<div class="col-lg-3 col-sm-6 col-xs-12">
			<div class="small-box bg-green">
				<div class="inner">
					<h3><?= $data['user'] ?></h3>
					<p>Jumlah Member</p>
				</div>
				<div class="icon">
					<i class="fa fa-users"></i>
				</div>
				<a href="index.php?admin=member" class="small-box-footer">More info
					<i class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-sm-6 col-xs-12">
			<div class="small-box bg-orange">
				<div class="inner">
					<h3><?= $data['zakat'] ?></h3>
					<p>Zakat</p>
				</div>
				<div class="icon">
					<i class="fa fa-handshake-o"></i>
				</div>
				<a href="index.php?admin=zakat" class="small-box-footer">More info
					<i class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-sm-6 col-xs-12">
			<div class="small-box bg-blue">
				<div class="inner">
					<h3><?= $data['konfirmasi'] ?></h3>
					<p>Konfirmasi Pembayaran</p>
				</div>
				<div class="icon">
					<i class="fa fa-money"></i>
				</div>
				<a href="index.php?admin=zakat" class="small-box-footer">More info
					<i class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-xs-12">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3><?= $data['pertanyaan'] ?></h3>
					<p>Pertanyaan</p>
				</div>
				<div class="icon">
					<i class="fa fa-question-circle"></i>
				</div>
				<a href="index.php?admin=pertanyaan" class="small-box-footer">More info
					<i class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
<?php

require_once __DIR__.'/../partials/admin/footer.php';

?>