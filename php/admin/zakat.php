<?php

defined('ONZAKAT') or die ('No Script Kiddies Please!');

if(isset($_SESSION['user'])) {
	if(!(bool)$_SESSION['user']->is_admin) header("location: ?member=dashboard");
} else {
	header("location: login.php");
}

if($_GET['admin'] == "konfirmasi-pembayaran" && isset($_GET['methode'], $_GET['id'])) {
	if(in_array($_GET['methode'], ['unapprove','approve'])) {
		Data\Zakat::konfirmasi($_GET['id'],$_GET['methode']);
	}
	header("location: index.php?admin=zakat");
	exit;
}

$page_title = "Kelola Zakat - ";
$page_description = "Lihat daftar member yang melakukan zakat.";

require_once __DIR__.'/../partials/admin/header.php';

$breadcrumbs = [
	["val"=>"<i class=\"fa fa-handshake-o\"></i> Kelola Zakat"],
];
$zakat = Data\Zakat::getData();

if(isset($_POST['delete'])) {
	if(Data\Zakat::delete($_POST['id'])) {
		$_SESSION['flash_message'] = [
			'title' => 'Sukses!',
			'class' => 'success',
			'message' => 'Berhasil menghapus zakat'
		];
		header("Refresh:0");
		exit;
	}
}


require_once __DIR__.'/../partials/admin/breadcrumbs.php';

?>
<!-- Main content -->
<section class="content">
	<?php alert(); ?>
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Kelola Zakat</h3>
			<div class="box-tools pull-right"></div>
		</div>
		<div class="box-body">
			<div class="table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th width="40">No</th>
							<th>Nama</th>
							<th>Tanggal</th>
							<th>Jenis Zakat</th>
							<th>Besar Zakat</th>
                            <th>Pembayaran</th>
                            <th>Bukti Pembayaran</th>
                            <th>Status</th>
							<th width="200">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$num = 1;
						foreach ($zakat->fetchAll(PDO::FETCH_OBJ) as $idx => $row): ?>
						<tr>
							<td><?= $num++ ?></td>
							<td><?= $row->user_nama ?></td>
							<td><?= date('d M Y', strtotime($row->pada)) ?></td>
							<td><?= $row->jenis_zakat ?></td>
							<td><?= number_format($row->jumlah, 0, ',', '.') ?>
							
							<?php
								if ($row->jenis_zakat == "penghasilan") {
									echo $row->monthly ? '(Perbulan)' : '(Pertahun)';
								}								
							?>

							</td>
							<td><?= strtoupper($row->jenis_bank) ?></td>
                            <td>
                                <?php if($row->bukti_foto): ?>
                                <a href="<?= SITE_URL.'/'.$row->bukti_foto ?>"><div class="thumbnail">
                                    <img src="<?= SITE_URL.'/'.$row->bukti_foto ?>" style="height:100px" alt="">
                                </div></a>
                                <?php else: ?>
                                <span class="label label-info">Belum di bayar</span>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php
                                    if($row->status == "unpaid") $cls = "danger";
									elseif($row->status == "waiting") $cls = "primary";
									elseif($row->status == "success") $cls = "success";
                                ?>
                                <span class="label label-<?= $cls ?>"><?= ucfirst($row->status) ?></span>
                            </td>							<td>
								<a href="index.php?admin=konfirmasi-pembayaran&methode=<?= $row->status == "success" ? 'unapprove' : 'approve' ?>&id=<?= $row->id ?>" class="btn btn-success btn-sm"><?= $row->status == "success" ? 'Batalkan' : 'Approve' ?></a>
								<form action="index.php?admin=zakat" style="display: inline-block;" method="post">
									<input type="hidden" name="delete">
									<input type="hidden" name="id" value="<?= $row->id ?>">
									<button class="btn btn-sm btn-danger" name="submit">Hapus</button>
								</form>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
<?php

require_once __DIR__.'/../partials/admin/footer.php';

?>