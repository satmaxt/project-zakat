<?php

defined('ONZAKAT') or die ('No Script Kiddies Please!');

if(isset($_SESSION['user'])) {
	if(!(bool)$_SESSION['user']->is_admin) header("location: ?member=dashboard");
} else {
	header("location: login.php");
}

if(!isset($_GET['id'])) {
	header("location: index.php?admin=pertanyaan");
}

$page_title = "Jawab Pertanyaan - ";
$page_description = "Jawab pertanyaan dari member.";

require_once __DIR__.'/../partials/admin/header.php';

$breadcrumbs = [
	["url"=>"index.php?admin=pertanyaan", "val"=>"<i class=\"fa fa-question-circle\"></i> Kelola Pertanyaan"],
	["val"=>"Jawab Pertanyaan"],
];

$pertanyaan = Data\AdminPertanyaan::getPertanyaan($_GET['id']);

if(isset($_POST['submit'])) {
	if(Data\AdminPertanyaan::update($_GET['id'], $_POST['jawaban'])) {
		$_SESSION['flash_message'] = [
			'title' => 'Sukses!',
			'class' => 'success',
			'message' => 'Berhasil menyimpan jawaban',
		];
		header("location: index.php?".$_SERVER['QUERY_STRING']);
		exit;
	}
}


require_once __DIR__.'/../partials/admin/breadcrumbs.php';

?>
<!-- Main content -->
<section class="content">
	<?php alert(); ?>
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Jawab Pertanyaan</h3>
			<div class="box-tools pull-right">
				<a href="index.php?admin=pertanyaan" class="btn btn-success btn-sm"><i class="fa fa-angle-left fa-fw"></i>Kembali</a>
			</div>
		</div>
		<div class="box-body">
			<form action="" method="POST">
				<div class="form-group">
					<label>Dari</label>
					<input type="text" class="form-control" value="<?= $pertanyaan->nama ?>" readonly>
				</div>
				<div class="form-group">
					<label>Judul Pertanyaan</label>
					<input type="text" class="form-control" value="<?= $pertanyaan->judul ?>" readonly>
				</div>
				<div class="form-group">
					<label>Pertanyaan</label>
					<textarea rows="3" class="form-control" readonly><?= $pertanyaan->detail ?></textarea>
				</div>
				<div class="form-group">
					<label>Jawaban Pertanyaan</label>
					<textarea name="jawaban" rows="3" class="form-control"><?= $pertanyaan->jawaban ?></textarea>
				</div>
				<div class="form-group">
					<button class="btn btn-success" name="submit">Berikan Jawaban</button>
				</div>
			</form>
		</div>
	</div>
</section>
<!-- /.content -->
<?php

require_once __DIR__.'/../partials/admin/footer.php';

?>