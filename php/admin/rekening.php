<?php

defined('ONZAKAT') or die ('No Script Kiddies Please!');

if(isset($_SESSION['user'])) {
	if(!(bool)$_SESSION['user']->is_admin) header("location: ?member=dashboard");
} else {
	header("location: login.php");
}

$page_title = "Kelola Rekening - ";
$page_description = "Lihat daftar rekening yang telah ditambahkan.";

require_once __DIR__.'/../partials/admin/header.php';

$breadcrumbs = [
	["val"=>"<i class=\"fa fa-credit-card\"></i> Kelola Rekening"],
];
$rekening = Data\AdminRekening::getData()->fetchAll(PDO::FETCH_OBJ);

if(isset($_POST['submit'])) {
	$res = Data\AdminRekening::storeData($_POST);
	if($res) {
		$_SESSION['flash_message'] = [
			'title' => 'Sukses!',
			'class' => 'success',
			'message' => 'Berhasil menambahkan rekening baru',
		];
	}
	header("Refresh:0");
	exit;
}

if(isset($_POST['delete'])) {
	if(Data\AdminRekening::deleteData($_POST['id'])) {
		$_SESSION['flash_message'] = [
			'title' => 'Sukses!',
			'class' => 'success',
			'message' => 'Berhasil menghapus rekening'
		];
		header("Refresh:0");
		exit;
	}
}


require_once __DIR__.'/../partials/admin/breadcrumbs.php';

?>
<!-- Main content -->
<section class="content">
	<?php alert(); ?>
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Kelola Rekening</h3>
			<div class="box-tools pull-right">
				<a href="#add-rekening" class="btn btn-sm btn-success" data-toggle="collapse"><i class="fa fa-pencil fa-fw"></i>Tambah Rekening</a>
			</div>
		</div>
		<div class="box-body">
			<div id="add-rekening" class="collapse">
				<form action="" method="post">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Jenis Bank</label>
								<input type="text" name="jenis" placeholder="Contoh: bca" class="form-control">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Nama Bank</label>
								<input type="text" name="nama" placeholder="Contoh: Bank Central Asia" class="form-control">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Atas Nama</label>
								<input type="text" name="pemilik" placeholder="Contoh: Hermanito" class="form-control">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Nomor Rekening</label>
								<input type="text" name="nomor" placeholder="Contoh: 8823758273" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<button class="btn btn-success" name="submit">Tambahkan</button>
					</div>
				</form>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th width="40">No</th>
							<th width="150">Jenis</th>
							<th>Bank</th>
							<th>Nama</th>
							<th>No Rekening</th>
							<th width="200">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$num = 1;
						foreach ($rekening as $idx => $row): ?>
						<tr>
							<td><?= $num++ ?></td>
							<td><?= $row->jenis ?></td>
							<td><?= $row->nama ?></td>
							<td><?= $row->pemilik ?></td>
							<td><?= $row->nomor ?></td>
							<td>
								<a href="index.php?admin=rekening-edit&id=<?= $row->id ?>" class="btn btn-success btn-sm">Edit</a>
								<form action="index.php?admin=rekening" style="display: inline-block;" method="post">
									<input type="hidden" name="delete">
									<input type="hidden" name="id" value="<?= $row->id ?>">
									<button class="btn btn-sm btn-danger">Hapus</button>
								</form>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
<?php

require_once __DIR__.'/../partials/admin/footer.php';

?>