<?php

defined('ONZAKAT') or die ('No Script Kiddies Please!');

if(isset($_SESSION['user'])) {
	if(!(bool)$_SESSION['user']->is_admin) header("location: ?member=dashboard");
} else {
	header("location: login.php");
}

$page_title = "Kelola Pertanyaan - ";
$page_description = "Lihat daftar pertanyaan yang diajukan.";

require_once __DIR__.'/../partials/admin/header.php';

$breadcrumbs = [
	["val"=>"<i class=\"fa fa-question-circle\"></i> Kelola Pertanyaan"],
];
$pertanyaan = Data\AdminPertanyaan::getData();

if(isset($_POST['delete'])) {
	if(Data\AdminPertanyaan::delete($_POST['id'], $_POST['user_id'])) {
		$_SESSION['flash_message'] = [
			'title' => 'Sukses!',
			'class' => 'success',
			'message' => 'Berhasil menghapus pertanyaan'
		];
	}
}


require_once __DIR__.'/../partials/admin/breadcrumbs.php';

?>
<!-- Main content -->
<section class="content">
	<?php alert(); ?>
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Daftar Pertanyaan</h3>
			<div class="box-tools pull-right"></div>
		</div>
		<div class="box-body">
			<div class="table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th width="40">No</th>
							<th width="150">Dari</th>
							<th width="100">Tanggal</th>
							<th width="200">Judul</th>
							<th>Pertanyaan</th>
							<th>Jawaban</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$num = 1;
						foreach ($pertanyaan->fetchAll(PDO::FETCH_OBJ) as $idx => $row): ?>
						<tr>
							<td><?= $num++ ?></td>
							<td><?= $row->nama ?></td>
							<td><?= date('d M Y', strtotime($row->pada)) ?></td>
							<td><?= $row->judul ?></td>
							<td><?= $row->detail ?></td>
							<td><?= !$row->jawaban ? '<span class="label label-default">Belum di jawab</span>' : $row->jawaban ?></td>
							<td>
								<a href="index.php?admin=jawab-pertanyaan&id=<?= $row->pertanyaan_id ?>" class="btn btn-success btn-sm"><?= !$row->jawaban ? 'Jawab' : 'Ubah Jawaban' ?></a>
								<form action="index.php?admin=pertanyaan" style="display: inline-block;" method="post">
									<input type="hidden" name="delete">
									<input type="hidden" name="id" value="<?= $row->pertanyaan_id ?>">
									<input type="hidden" name="user_id" value="<?= $row->user_id ?>">
									<button class="btn btn-sm btn-danger" name="submit">Hapus</button>
								</form>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
<?php

require_once __DIR__.'/../partials/admin/footer.php';

?>