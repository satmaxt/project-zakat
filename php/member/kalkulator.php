<?php

defined('ONZAKAT') or die('No Script Kiddies Please!');

if(!isset($_SESSION['user'])) {
	header("location: login.php");
}

$page_title = "Kalkulator Zakat - ";
$page_description = "Hitung jumlah zakat anda sebelum mengeluarkan zakat.";

$breadcrumbs = [
	["val"=>"<i class=\"fa fa-calculator fa-fw\"></i> Kalkulator Zakat"]
];

require_once __DIR__.'/../partials/admin/header.php';
require_once __DIR__.'/../partials/member/breadcrumbs.php';

?>
<section class="content">
	<?php alert() ?>
	<form action="index.php?member=bayar-zakat" id="form-kalkulator-zakat" method="post">
		<input type="hidden" name="type">
		<div class="nav-tabs-custom" id="kalkulator-tab">
			<ul class="nav nav-tabs">
				<li>
					<a href="#penghasilan" type="penghasilan" data-toggle="tab">Zakat Penghasilan</a>
				</li>
				<li>
					<a href="#perdagangan" type="perdagangan" data-toggle="tab">Zakat Perdagangan</a>
				</li>
				<li>
					<a href="#tabungan" type="tabungan" data-toggle="tab">Zakat Tabungan</a>
				</li>
				<li>
					<a href="#emas" type="emas" data-toggle="tab">Zakat Emas</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane fade" id="penghasilan">
					<div class="row">
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Penghasilan per Bulan</label>
								<input type="number" value="0" min="0" name="z_1_penghasilan" class="form-control">
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Pendapatan Lainnya (per Bulan)</label>
								<input type="number" value="0" min="0" name="z_1_pendapatan_lain" class="form-control">
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Pengeluaran (per Bulan)</label>
								<input type="number" value="0" min="0" name="z_1_pengeluaran" class="form-control">
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Harga Enas Saat Ini (per gr)</label>
								<input type="number" class="harga-beras form-control" readonly>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Nishab (85gr emas)</label>
								<input type="number" class="nishab-beras form-control" readonly>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Apakah wajib bayar zakat?</label>
								<input type="text" name="z_1_wajib_zakat" class="form-control" readonly>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Jml. Zakat Penghasilan (per Bulan)</label>
								<input type="number" value="0" min="0" name="z_1_zakat_bln" class="form-control" readonly>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Jml. Zakat Penghasilan (per Tahun)</label>
								<input type="number" value="0" min="0" name="z_1_zakat_thn" class="form-control" readonly>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="perdagangan">
					<div class="row">
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Perputaran Modal Selama 1 Tahun</label>
								<input type="number" value="0" min="0" name="z_2_modal" class="form-control">
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Keuntungan Selama 1 Tahun</label>
								<input type="number" value="0" min="0" name="z_2_untung" class="form-control">
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Piutang Dagang</label>
								<input type="number" value="0" min="0" name="z_2_piutang" class="form-control">
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Hutang Jatuh Tempo</label>
								<input type="number" value="0" min="0" name="z_2_hutang" class="form-control">
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Kerugian Selama 1 Tahun</label>
								<input type="number" value="0" min="0" name="z_2_rugi" class="form-control">
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Harga Emas Saat Ini</label>
								<input type="number" class="harga-emas form-control" readonly>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Nishab (85gram)</label>
								<input type="number" class="nishab-emas form-control" readonly>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Wajib bayar zakat perdagangan?</label>
								<input type="text" name="z_2_wajib_zakat" class="form-control" readonly>
							</div>
						</div>
						<div class="col-lg-4 col-sm-6">
							<div class="form-group">
								<label>Jml. Zakat Perdagangan</label>
								<input type="number" value="0" min="0" name="z_2_zakat" class="form-control" readonly>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="tabungan">
					<div class="row">
						<div class="col-md-4 col-sm-6">
							<div class="form-group">
								<label>Saldo Tabungan</label>
								<input type="number" value="0" min="0" name="z_3_saldo" class="form-control">
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="form-group">
								<label>Bagi Hasil</label>
								<input type="number" value="0" min="0" name="z_3_bagi_hasil" class="form-control">
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="form-group">
								<label>Harga Emas Saat Ini</label>
								<input type="number" class="harga-emas form-control" readonly>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="form-group">
								<label>Nishab (85gram)</label>
								<input type="number" class="nishab-emas form-control" readonly>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="form-group">
								<label>Wajib Bayar Zakat Emas?</label>
								<input type="text" name="z_3_wajib_zakat" class="form-control" readonly>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="form-group">
								<label>Jml. Zakat Tabungan</label>
								<input type="number" value="0" min="0" name="z_3_zakat" value="625000" class="form-control" readonly>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="emas">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Jml. Emas yg Dimiliki (gram)</label>
								<input type="number" value="0" min="0" name="z_4_emas" class="form-control">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Harga Emas Saat Ini</label>
								<input type="number" class="harga-emas form-control" readonly>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Nishab (85gram)</label>
								<input type="number" class="nishab-emas form-control" readonly>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Wajib Bayar Zakat Emas?</label>
								<input type="text" name="z_4_wajib_zakat" class="form-control" readonly>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label>Jml. Zakat Emas</label>
								<input type="number" value="0" min="0" name="z_4_zakat" class="form-control" readonly>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<button name="submit" class="btn btn-success">Bayar Zakat</button>
				</div>
			</div>
		</div>
		<div class="form-group">
			<span class="help-block">Sumber harga beras: <a href="https://bps.go.id/LinkTableDinamis/view/id/963" target="_blank" rel="nofollow">Badan Pusat Statistika</a></span>
			<span class="help-block">Sumber harga emas: <a href="https://www.indogold.com/harga-emas-hari-ini" target="_blank" rel="nofollow">IndoGold</a></span>
		</div>
	</form>
</section>

<?php 

$footer_scripts = "
	<script>
		var nishabEmas = 85;
		var nishabBeras = 520;
		var hargaEmas = ".updatePrice('emas').";
		var hargaBeras = ".updatePrice('beras').";
		var nishabBerasHarga = nishabBeras*hargaBeras;
		var nishabEmasHarga = nishabEmas*hargaEmas;
	</script>
	<script src=\"".SITE_URL."/assets/dist/js/calculator.js\"></script>
";

require_once __DIR__.'/../partials/admin/footer.php';

?>