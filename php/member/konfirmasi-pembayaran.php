<?php

defined('ONZAKAT') or die('No Script Kiddies Please!');

if(!isset($_SESSION['user'])) {
	header("location: login.php");
}

if(!isset($_GET['id'])) header("location: index.php?member=kalkulator");

$rekening = Core\DB::conn()->query("SELECT * FROM rekening WHERE deleted = 0", PDO::FETCH_OBJ)->fetchAll();
$data = Data\Zakat::getData($_GET['id'], user()->id);

if($data->rowCount() < 1) {
	$_SESSION['flash_message'] = [
		'title'	 => 'Error!',
		'class' => 'warning',
		'message' => 'Konfirmasi zakat yang anda akses tidak ditemukan.',
	];
	header("location: index.php?member=kalkulator");
	exit;
} else {
	$data = $data->fetch(PDO::FETCH_OBJ);
}

if($data->status == "success") {
	$_SESSION['flash_message'] = [
		"title" => "Info",
		"class" => "info",
		"message" => "Tidak bisa mengakses halaman karena status pembayaran sudah sukses"
	];
	header("location: index.php?member=kalkulator");
	exit;
}

$page_title = "Konfirmasi Pembayaran - ";
$page_description = "Unggah bukti pembayaran disini.";

$breadcrumbs = [
	[
		"url"=>"index.php?member=pembayaran",
		"val"=>"<i class=\"fa fa-reply fa-fw\"></i> Konfrimasi Pembayaran"
	],
	[
		"val"=>"Konfirmasi Pembayaran"
	]
];

if(isset($_POST['submit']) || isset($_POST['change_payment'])) {
	$res = Data\BayarZakat::confirmPembayaran($_GET['id'], ['post'=>$_POST, 'files'=>$_FILES], user()->id);
	header("Refresh:0");
	exit;
}

require_once __DIR__.'/../partials/admin/header.php';
require_once __DIR__.'/../partials/member/breadcrumbs.php';

?>

<section class="content">
	<?php alert() ?>
	<div class="row">
		<div class="col-md-offset-2 col-md-8">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Pembayaran Zakat</h3>
					<div class="box-tools">
						<a href="index.php?member=pembayaran" class="btn btn-sm btn-success"><i class="fa fa-angle-left fa-fw"></i>Kembali</a>
					</div>
				</div>
				<div class="box-body">
					<form action="" method="post" id="form-pembayaran" enctype="multipart/form-data">
						<div class="form-group">
							<label>Jenis Zakat</label>
							<input type="text" name="jenis_zakat" value="<?= $data->jenis_zakat ?>" class="form-control" readonly>
						</div>
						<div class="form-group form-icheck">
							<label>Besar Zakat</label>
							<input type="text" class="form-control" value="<?= number_format($data->jumlah, 0, ',', '.') ?>" readonly>
						</div>
						<div class="form-group form-icheck">
							<label>Rekening tujuan pembayaran</label>
							<span class="help-block">Silahkan melakukan transfer dana ke rekening di bawah ini. Lalu unggah bukti pembayarannya.</span>
							<div class="well">
								<strong><?= $data->nama_bank ?></strong><br>
								Nomor Rekening: <?= $data->nomor_rekening ?> <br>
								Atas Nama: <?= $data->pemilik_rekening ?>
							</div>
						</div>
						<div id="ubah-metode-pembayaran" class="collapse">
							<div class="form-group form-icheck">
								<label>Ubah Metode Pembayaran</label>
								<br />
								<?php foreach ($rekening as $row): ?>
									<input type="radio" name="metode_pembayaran" value="<?= $row->jenis ?>" id="<?= $row->jenis ?>">
									<label for="<?= $row->jenis ?>"><?= strtoupper($row->jenis) ?></label>
								<?php endforeach ?>
								<button name="change_payment" class="btn btn-success btn-sm">Simpan metode pembayaran</button>
							</div>
							<div class="form-group">
								<div class="well collapse" id="info-metode-pembayaran">
									
								</div>
							</div>
						</div>
						<?php if ($data->bukti_foto): ?>
							<div class="form-group">
								<label>Foto yang sudah diunggah</label>
								<div class="thumbnail">
									<img src="<?= SITE_URL."/".$data->bukti_foto ?>" alt="">
								</div>
							</div>
						<?php endif ?>
						<div class="form-group">
							<label>Foto bukti pembayaran</label>
							<input type="file" name="bukti_pembayaran" class="form-control">
						</div>
						<div class="form-group">
							<button class="btn btn-success" name="submit">Unggah Bukti Pembayaran</button>
							<a href="#ubah-metode-pembayaran" data-toggle="collapse" class="btn btn-link"><span class="text-success">Ubah metode pembayaran</span></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 

$dataBank = array();

foreach($rekening as $row) {
	$dataBank[$row->jenis] = [
		"bank" => $row->nama,
		"rek" => $row->nomor,
		"name" => $row->pemilik,
	];
}

$footer_scripts = "
	<script>
		var dataBank = ".json_encode($dataBank)."
	</script>
	<script src=\"".SITE_URL."/assets/dist/js/pembayaran-1.js\"></script>
";

require_once __DIR__.'/../partials/admin/footer.php';

?>