<?php

defined('ONZAKAT') or die ('No Script Kiddies Please!');

if(!isset($_SESSION['user'])) {
	header("location: login.php");
}

$page_title = "Pertanyaan - ";
$page_description = "Lihat daftar pertanyaan yang diajukan.";

require_once __DIR__.'/../partials/admin/header.php';

$breadcrumbs = [
	["val"=>"<i class=\"fa fa-question-circle\"></i> Kelola Pertanyaan"],
];
$pertanyaan = Data\AdminPertanyaan::getData(user()->id);

if(isset($_POST['submit'])) {
	if(Data\AdminPertanyaan::storeData(user()->id, $_POST)) {
		$_SESSION['flash_message'] = [
			'title'	 => 'Sukses!',
			'class' => 'success',
			'message' => 'Berhasil mengajukan pertanyaan'
		];
		header("location: index.php?member=pertanyaan");
		exit;
	}
}

if(isset($_POST['delete'])) {
	if(Data\AdminPertanyaan::delete($_POST['id'], user()->id)) {
		$_SESSION['flash_message'] = [
			'title' => 'Sukses!',
			'class' => 'success',
			'message' => 'Berhasil menghapus pertanyaan'
		];
		header("location: index.php?member=pertanyaan");
		exit;
	}
}


require_once __DIR__.'/../partials/member/breadcrumbs.php';

?>
<!-- Main content -->
<section class="content">
	<?php alert(); ?>
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Daftar Pertanyaan</h3>
			<div class="box-tools pull-right">
				<a href="#add-pertanyaan" data-toggle="collapse" class="btn btn-success btn-sm"><i class="fa fa-question-circle fa-fw"></i> Ajukan Pertanyaan</a>
			</div>
		</div>
		<div class="box-body">
			<div id="add-pertanyaan" class="collapse">
				<form action="" method="post">
					<div class="form-group">
						<label>Judul Pertanyaan</label>
						<input type="text" name="judul" class="form-control">
					</div>
					<div class="form-group">
						<label>Pertanyaan</label>
						<textarea name="detail" rows="3" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<button class="btn btn-success" name="submit">Ajukan Pertanyaan</button>
					</div>
				</form>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th width="40">No</th>
							<th width="100">Tanggal</th>
							<th width="200">Judul</th>
							<th>Pertanyaan</th>
							<th>Jawaban</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$num = 1;
						foreach ($pertanyaan->fetchAll(PDO::FETCH_OBJ) as $idx => $row): ?>
						<tr>
							<td><?= $num++ ?></td>
							<td><?= date('d M Y', strtotime($row->pada)) ?></td>
							<td><?= $row->judul ?></td>
							<td><?= $row->detail ?></td>
							<td><?= !$row->jawaban ? '<span class="label label-default">Belum di jawab</span>' : $row->jawaban ?></td>
							<td>
								<form action="" style="display: inline-block;" method="post">
									<input type="hidden" name="delete">
									<input type="hidden" name="id" value="<?= $row->pertanyaan_id ?>">
									<button class="btn btn-sm btn-danger" name="submit">Hapus</button>
								</form>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				<center><?= $pertanyaan->rowCount() < 1 ? 'Belum ada pertanyaan yang diajukan' : '' ?></center>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
<?php

require_once __DIR__.'/../partials/admin/footer.php';

?>