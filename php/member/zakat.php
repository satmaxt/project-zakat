<?php

defined('ONZAKAT') or die('No Script Kiddies Please!');

if(!isset($_SESSION['user'])) {
	header("location: login.php");
}

$page_title = "Zakat";
$page_description = "Lihat riwayat zakat yang sudah dibayar.";
$zakat = Data\Zakat::getData(false,user()->id,"A.status = 'success'")->fetchAll(PDO::FETCH_OBJ);

$breadcrumbs = [
	["val"=>"<i class=\"fa fa-handshake-o fa-fw\"></i> Zakat"]
];

require_once __DIR__.'/../partials/admin/header.php';
require_once __DIR__.'/../partials/member/breadcrumbs.php';

?>
<!-- Main content -->
<section class="content">
	<?php alert(); ?>
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Zakat</h3>
			<div class="box-tools pull-right">
				<a href="index.php?member=kalkulator" class="btn btn-sm btn-success"><i class="fa fa-handshake-o fa-fw"></i>Bayar Zakat</a>
			</div>
		</div>
		<div class="box-body">
			<div class="table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th width="40">No</th>
							<th>Tanggal</th>
							<th>Jenis Zakat</th>
							<th>Besar Zakat</th>
                            <th>Pembayaran</th>
                            <th>Bukti Pembayaran</th>
                            <th>Status</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$num = 1;
						foreach ($zakat as $idx => $row): ?>
						<tr>
							<td><?= $num++ ?></td>
							<td><?= date('d M Y', strtotime($row->pada)) ?></td>
							<td><?= $row->jenis_zakat ?></td>
							<td><?= number_format($row->jumlah, 0, ',', '.') ?>

							<?php
								if ($row->jenis_zakat == "penghasilan") {
									echo $row->monthly ? '(Perbulan)' : '(Pertahun)';
								}								
							?>

							</td>
							<td><?= strtoupper($row->jenis_bank) ?></td>
                            <td>
                                <?php if($row->bukti_foto): ?>
                                <a href="<?= SITE_URL.'/'.$row->bukti_foto ?>"><div class="thumbnail">
                                    <img src="<?= SITE_URL.'/'.$row->bukti_foto ?>" style="height:100px" alt="">
                                </div></a>
                                <?php else: ?>
                                <span class="label label-info">Belum di bayar</span>
                                <?php endif; ?>
                            </td>
                            <td>
                                <span class="label label-success"><?= ucfirst($row->status) ?></span>
                            </td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
                <?php if(count($zakat) < 1): ?>
                <center><span>Belum ada riwayat pembayaran zakat</span></center>
                <?php endif; ?>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
<?php

require_once __DIR__.'/../partials/admin/footer.php';

?>