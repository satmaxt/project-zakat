<?php

defined('ONZAKAT') or die('No Script Kiddies Please!');

if(!isset($_SESSION['user'])) {
	header("location: login.php");
}

if(isset($_POST['jenis_zakat'])) {
	$pembayaran = Data\BayarZakat::storePembayaran($_POST);
	if($pembayaran) {
		header("location: index.php?member=konfirmasi-pembayaran&id=".$pembayaran);
		exit;
	} else {
		header("location: index.php?member=kalkulator");
	}
}

if(isset($_POST['submit'])) {
	if(in_array($_POST['type'], ['penghasilan','tabungan','emas','perdagangan'])) {
		$data = Data\BayarZakat::generatePembayaran($_POST);
	} else {
		header("location: index.php?member=kalkulator");
	}
} else {
	header("location: index.php?member=kalkulator");
}

$page_title = "Bayar Zakat - ";
$page_description = "Bayar zakat online hasil hitung dari kalkulator zakat.";
$rekening = Core\DB::conn()->query("SELECT * FROM rekening WHERE deleted = 0", PDO::FETCH_OBJ)->fetchAll();

$breadcrumbs = [
	[
		"url"=>"index.php?member=kalkulator",
		"val"=>"<i class=\"fa fa-handshake-o fa-fw\"></i> Bayar Zakat"
	],
	[
		"val"=>"<i class=\"fa fa-handshake-o fa-fw\"></i> Bayar Zakat"
	]
];

require_once __DIR__.'/../partials/admin/header.php';
require_once __DIR__.'/../partials/member/breadcrumbs.php';

?>

<section class="content">
<?php alert() ?>
	<div class="row">
		<div class="col-md-offset-3 col-md-6">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Pembayaran Zakat</h3>
					<div class="box-tools">
						<a href="index.php?member=kalkulator" class="btn btn-sm btn-success"><i class="fa fa-angle-left fa-fw"></i>Kalkulator Zakat</a>
					</div>
				</div>
				<div class="box-body">
					<form action="" id="form-pembayaran" method="post">
						<div class="form-group">
							<label>Jenis Zakat</label>
							<input type="text" name="jenis_zakat" value="<?= $data['type'] ?>" class="form-control" readonly>
						</div>
						<?php if ($data['type'] == "penghasilan"): ?>
							<div class="form-group form-icheck">
								<label>Pembayaran</label>
								<br />
								<input type="radio" name="pembayaran" price="<?= $data['zakat_bln'] ?>" value="bln" id="pembayaran_bln">
								<label for="pembayaran_bln">Perbulan</label>
								<input type="radio" id="pembayaran_thn" price="<?= $data['zakat_thn'] ?>" name="pembayaran" value="thn">
								<label for="pembayaran_thn">Pertahun</label>
							</div>
						<?php endif ?>
						<div class="form-group">
							<label>Besar Zakat</label>
							<input type="number" name="besar_zakat" value="<?= $data['type'] !== "penghasilan" ? $data['besar_zakat'] : '' ?>" class="form-control" readonly>
						</div>
						<div class="form-group form-icheck">
							<label>Metode Pembayaran</label>
							<br />
							<?php foreach ($rekening as $row): ?>
								<input type="radio" name="metode_pembayaran" value="<?= $row->jenis ?>" id="<?= $row->jenis ?>">
								<label for="<?= $row->jenis ?>"><?= strtoupper($row->jenis) ?></label>
							<?php endforeach ?>
						</div>
						<div class="form-group">
							<div class="well collapse" id="info-metode-pembayaran">
								
							</div>
						</div>
						<div class="form-group">
							<button class="btn btn-success">Bayar Zakat</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 

$dataBank = array();

foreach($rekening as $row) {
	$dataBank[$row->jenis] = [
		"bank" => $row->nama,
		"rek" => $row->nomor,
		"name" => $row->pemilik,
	];
}

$footer_scripts = "
	<script>
		var dataBank = ".json_encode($dataBank)."
	</script>
	<script src=\"".SITE_URL."/assets/dist/js/pembayaran-1.js\"></script>
";

require_once __DIR__.'/../partials/admin/footer.php';

?>