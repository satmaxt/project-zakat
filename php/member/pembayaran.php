<?php

defined('ONZAKAT') or die('No Script Kiddies Please!');

if(!isset($_SESSION['user'])) {
	header("location: login.php");
}

$page_title = "Konfirmasi Pembayaran";
$page_description = "Konfirmasi pembayaran zakat dengan mengunggah foto bukti pembayaran";
$pembayaran = Data\Zakat::getData(false,user()->id,"A.status != 'success'")->fetchAll(PDO::FETCH_OBJ);

$breadcrumbs = [
	["val"=>"<i class=\"fa fa-reply fa-fw\"></i> Konfrimasi Pembayaran"]
];

require_once __DIR__.'/../partials/admin/header.php';
require_once __DIR__.'/../partials/member/breadcrumbs.php';

?>
<!-- Main content -->
<section class="content">
	<?php alert(); ?>
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Konfirmasi Pembayaran</h3>
			<div class="box-tools pull-right">
				<a href="index.php?member=kalkulator" class="btn btn-sm btn-success"><i class="fa fa-handshake-o fa-fw"></i>Bayar Zakat</a>
			</div>
		</div>
		<div class="box-body">
			<div class="table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th width="40">No</th>
							<th>Tanggal</th>
							<th>Jenis Zakat</th>
							<th>Besar Zakat</th>
                            <th>Pembayaran</th>
                            <th>Bukti Pembayaran</th>
                            <th>Status</th>
							<th width="200">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$num = 1;
						foreach ($pembayaran as $idx => $row): ?>
						<tr>
							<td><?= $num++ ?></td>
							<td><?= date('d M Y', strtotime($row->pada)) ?></td>
							<td><?= $row->jenis_zakat ?></td>
							<td><?= number_format($row->jumlah, 0, ',', '.') ?>

							<?php
								if ($row->jenis_zakat == "penghasilan") {
									echo $row->monthly ? '(Perbulan)' : '(Pertahun)';
								}								
							?>

							</td>
							<td><?= strtoupper($row->jenis_bank) ?></td>
                            <td>
                                <?php if($row->bukti_foto): ?>
                                <a href="<?= SITE_URL.'/'.$row->bukti_foto ?>"><div class="thumbnail">
                                    <img src="<?= SITE_URL.'/'.$row->bukti_foto ?>" style="height:100px" alt="">
                                </div></a>
                                <?php else: ?>
                                <span class="label label-info">Belum di bayar</span>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php
                                    if($row->status == "unpaid") $cls = "danger";
                                    elseif($row->status == "waiting") $cls = "primary";
                                ?>
                                <span class="label label-<?= $cls ?>"><?= ucfirst($row->status) ?></span>
                            </td>
							<td>
								<a href="index.php?member=konfirmasi-pembayaran&id=<?= $row->id ?>" class="btn btn-success btn-sm">Konfirmasi Pembayaran</a>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
                <?php if(count($pembayaran) < 1): ?>
                <center><span>Tidak ada pembayaran yang menunggu konfirmasi</span></center>
                <?php endif; ?>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
<?php

require_once __DIR__.'/../partials/admin/footer.php';

?>