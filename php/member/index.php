<?php

defined('ONZAKAT') or die ('No Script Kiddies Please!');

if(!isset($_SESSION['user'])) {
	header("location: login.php");
}

$page_title = "Beranda - ";
$data = memberDashboardData();

require_once __DIR__.'/../partials/admin/header.php';


require_once __DIR__.'/../partials/member/breadcrumbs.php';

?>
<!-- Main content -->
<section class="content">
	<div class="panel collapse" id="welcome">
		<div class="panel-body ">
			Selamat datang di area beranda anda. Di sini anda dapat mengelola pembayaran zakat, melihat riwayat pembayaran zakat, dan
                        konsultasi bersama admin.
		</div>
	</div>
	<div class="row">
        <div class="col-lg-4 col-sm-6 col-xs-12">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?= $data['zakat'] ?></h3>
                    <p>Zakat</p>
                </div>
                <div class="icon">
                    <i class="fa fa-handshake-o"></i>
                </div>
                <a href="index.php?member=zakat" class="small-box-footer">More info
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-xs-12">
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3><?= $data['konfirmasi'] ?></h3>
                    <p>Konfirmasi Pembayaran</p>
                </div>
                <div class="icon">
                    <i class="fa fa-money"></i>
                </div>
                <a href="index.php?member=pembayaran" class="small-box-footer">More info
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-12">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= $data['pertanyaan'] ?></h3>
                    <p>Pertanyaan</p>
                </div>
                <div class="icon">
                    <i class="fa fa-question-circle"></i>
                </div>
                <a href="index.php?member=pertanyaan" class="small-box-footer">More info
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">Status Pembayaran</h3>
			<div class="box-tools pull-right">
				<a href="index.php?member=kalkulator" class="btn btn-sm btn-success">Kalkulator Zakat</a>
			</div>
		</div>
		<div class="box-body">
			<div class="table-responsive">
			<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th width="40">No</th>
							<th>Tanggal</th>
							<th>Jenis Zakat</th>
							<th>Besar Zakat</th>
                            <th>Pembayaran</th>
                            <th>Bukti Pembayaran</th>
                            <th>Status</th>
							<th width="200">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$num = 1;
						foreach ($data['pembayaran'] as $idx => $row): ?>
						<tr>
							<td><?= $num++ ?></td>
							<td><?= date('d M Y', strtotime($row->pada)) ?></td>
							<td><?= $row->jenis_zakat ?></td>
							<td><?= number_format($row->jumlah, 0, ',', '.') ?>

							<?php
								if ($row->jenis_zakat == "penghasilan") {
									echo $row->monthly ? '(Perbulan)' : '(Pertahun)';
								}								
							?>

							</td>
							<td><?= strtoupper($row->jenis_bank) ?></td>
                            <td>
                                <?php if($row->bukti_foto): ?>
                                <a href="<?= SITE_URL.'/'.$row->bukti_foto ?>"><div class="thumbnail">
                                    <img src="<?= SITE_URL.'/'.$row->bukti_foto ?>" style="height:100px" alt="">
                                </div></a>
                                <?php else: ?>
                                <span class="label label-info">Belum di bayar</span>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php
                                    if($row->status == "unpaid") $cls = "danger";
                                    elseif($row->status == "waiting") $cls = "primary";
                                ?>
                                <span class="label label-<?= $cls ?>"><?= ucfirst($row->status) ?></span>
                            </td>
							<td>
								<a href="index.php?member=konfirmasi-pembayaran&id=<?= $row->id ?>" class="btn btn-success btn-sm">Konfirmasi Pembayaran</a>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
                <?php if(count($data['pembayaran']) < 1): ?>
                <center><span>Tidak ada pembayaran yang menunggu konfirmasi</span></center>
                <?php endif; ?>
			</div>
		</div>
		<div class="box-footer">
			<a href="index.php?member=pembayaran" class="btn btn-success">Lihat Lebih Banyak</a>
		</div>
	</div>
</section>
<!-- /.content -->
<?php

require_once __DIR__.'/../partials/admin/footer.php';

?>